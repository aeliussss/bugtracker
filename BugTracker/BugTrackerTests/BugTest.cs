﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker;
using System.Data.SqlTypes;

namespace BugTrackerTests
{
    /// <summary>
    /// BugTest Test Class
    /// </summary>
    [TestClass]
    public class BugTest
    {
        /// <summary>
        /// Test the bug's getters.
        /// </summary>
        [TestMethod]
        public void BugTestGetters()
        {
            // arrange test variables
            int expected = 0;
            SqlDateTime dateCreated = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));

            // call the class to be tested
            Bug bug = new Bug(0, "Some Annoying Bug!", 0, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED");

            // set the actual variables
            int actual = bug.GetId();

            // use assert to test the id of the bug
            Assert.AreEqual(expected, actual, "The ID of the bug has not been retrieved correctly.");
        }
        /// <summary>
        /// Tests the Bug setters.
        /// </summary>
        [TestMethod]
        public void TestSetSetters()
        {
            // arrange test variables
            int expected = 3;
            SqlDateTime dateCreated = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));

            // call the class to be tested
            Bug bug = new Bug(0, "Some Annoying Bug!", 0, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED");

            // call the method to be tested
            bug.SetId(3);

            // set the actual variable
            int actual = bug.GetId();
            
            // use assert to test the id of the bug
            Assert.AreEqual(expected, actual, "The ID of the bug has not been set by the SetId method.");
        }
    }
}
