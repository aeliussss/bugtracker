﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker;
using System.Data.SqlTypes;
using System.Linq;

namespace BugTrackerTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class BugTrackerTest
    {
        /// <summary>
        /// Bugs the tracker_ populate bug list_ test bug being edited being null.
        /// </summary>
        [TestMethod]
        public void BugTracker_PopulateBugList_TestBugBeingEditedBeingNull()
        {
            //arrange variables to test
            List<Bug> BugList = new List<Bug>();
            int expected = 1;
            Bug BugBeingEdited;
            SqlDateTime dateCreated = new SqlDateTime();

            // add bug classes to test list
            BugList.Add(new Bug(0, "Some Annoying Bug!", 0, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED"));
            BugList.Add(new Bug(1, "Some Another Annoying Bug!", 1, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED"));
            BugList.Add(new Bug(2, "Yet Another Bug!", 2, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED"));

            // create application logic
            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == expected)
                {
                    BugBeingEdited = BugList.ElementAt(i);
                }
            }

            // fire populateBugList logic
            BugList.Clear();

            BugList.Add(new Bug(0, "Some Annoying Bug!", 0, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED"));
            BugList.Add(new Bug(1, "Some Another Annoying Bug!", 1, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED"));
            BugList.Add(new Bug(2, "Yet Another Bug!", 2, dateCreated, "ewkdjnwekj8732bjwedhxb", "UNCONFIRMED"));

            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == expected)
                {
                    BugBeingEdited = BugList.ElementAt(i);
                }
            }

            // see if bugbeingedited fires an exception
            int actual = BugList.ElementAt(expected).GetId();
            Assert.AreEqual(expected, actual, "populateBugList causes the BugBeingEdited variable to become null");
        }

        /// <summary>
        /// Bugs the tracker_add bug_ verify lines input.
        /// </summary>
        [TestMethod]
        public void BugTracker_addBug_VerifyLinesInput()
        {
            //arrange variables to test
            string input = "dwkjn";
            int expected = 5;
            int actual;

            //create application logic
            try
            {
                actual = int.Parse(input);
                Console.WriteLine("{0} --> {1}", input, actual);
            } catch (FormatException)
            {
                actual = int.Parse("5");
                Console.WriteLine("{0}: Bad Format", input);
            } catch (OverflowException)
            {
                actual = int.Parse("5");
                Console.WriteLine("{0}: Overflow", input);
            }

            //test if exception is thrown if so the test fails
            Assert.AreEqual(expected, actual, "Input converted with an exception.");
        }

        /// <summary>
        /// Bugs the tracker_add bug_ verify fields empty.
        /// </summary>
        [TestMethod]
        public void BugTracker_addBug_VerifyFieldsEmpty()
        {
            // arrange variables to test
            string input = "";
            bool expected = true;

            // create application logic to see if string is empty
            bool actual = false;

            if (string.Empty == input)
            {
                actual = true;
            }

            // assert variable to see if string passes test
            Assert.AreEqual(expected, actual, "String was not empty or exception was thrown");
        }
    }
}
