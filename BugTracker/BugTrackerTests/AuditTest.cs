﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker;

namespace BugTrackerTests
{
    /// <summary>
    /// AuditTest Test Class
    /// </summary>
    [TestClass]
    public class AuditTest
    {
        /// <summary>
        /// Audits the format lines_ format lines.
        /// </summary>
        [TestMethod]
        public void AuditFormatLines_FormatLines()
        {
            // arrange variables for testing format lines
            string expected = "1 - 100";
            Audit audit = new Audit(0,0,0,"UNCONFIRMED", "Project Name", "SomeClass", "SomeMethod", "SomeFile.cs", "Some code here...", 1, 100, "d2938dj2n3lidu2jl");

            /*
            // create string to test
            string lines = audit.GetStartLine() + " - " + audit.GetEndLine();
            */

            // implement method in audit class
            string lines = audit.GetFormattedLines();

            // compare to expected outcome
            string actual = lines;
            Assert.AreEqual(expected, actual, "Lines not formatted correctly.");
        }
        /// <summary>
        /// Audits the test get identifier.
        /// </summary>
        [TestMethod]
        public void AuditTestGetId()
        {
            // arrange variables to test getters
            int expected = 0;
            Audit audit = new Audit(0, 0, 0, "UNCONFIRMED", "Project Name", "SomeClass", "SomeMethod", "SomeFile.cs", "Some code here...", 1, 100, "d2938dj2n3lidu2jl");

            // test getters by comparing the actual output with expected
            int actual = audit.GetId();
            Assert.AreEqual(expected, actual, "GetId did not return the correct id.");
        }
        /// <summary>
        /// Audits the test set identifier.
        /// </summary>
        [TestMethod]
        public void AuditTestSetId()
        {
            // arrange variables to test setters
            int expected = 3;
            Audit audit = new Audit(0, 0, 0, "UNCONFIRMED", "Project Name", "SomeClass", "SomeMethod", "SomeFile.cs", "Some code here...", 1, 100, "d2938dj2n3lidu2jl");

            // invoke the set method we are testing 
            audit.SetId(3);

            // test setters by comparing the actual output with expected getter
            int actual = audit.GetId();
            Assert.AreEqual(expected, actual, "The ID has not been set by the SetId method.");
        }
    }
}
