﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;

namespace BugTrackerTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ContributorTest
    {
        /// <summary>
        /// Contributors the full name of the get full name_ get.
        /// </summary>
        [TestMethod]
        public void ContributorGetFullName_GetFullName()
        {
            // set arrange variables and initialise contributor class instance
            string expected = "Andy Carroll";
            SqlDateTime lastSignIn = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            Contributor contributor = new Contributor(0, "Andy", "Carroll", "Author", 1431, lastSignIn);

            // set the actual answer
            string actual = contributor.GetFullName();

            // check if he full name method outputs the formatted name correctly
            Assert.AreEqual(expected, actual, "Full name does not match.");
        }

        /// <summary>
        /// Contributors the set identifier.
        /// </summary>
        [TestMethod]
        public void ContributorSetID()
        {
            // set arrange variables and initialise contributor class instance
            int expected = 27203;
            SqlDateTime lastSignIn = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            Contributor contributor = new Contributor(0, "Andy", "Carroll", "Author", 1431, lastSignIn);

            // set the id and therefore test the method
            contributor.SetId(27203);

            // use the get id method to get the acutal answer and compare with the expected ID
            int actual = contributor.GetId();
            Assert.AreEqual(expected, actual, "SetId did not assign the value to the class successfully.");
        }
        /// <summary>
        /// Contributors the get identifier.
        /// </summary>
        [TestMethod]
        public void ContributorGetID()
        {
            // set arrange variables and initialise contributor class instance
            int expected = 0;
            SqlDateTime lastSignIn = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            Contributor contributor = new Contributor(0, "Andy", "Carroll", "Author", 1431, lastSignIn);

            // get the id
            int actual = contributor.GetId();

            // compare the expected id with actual id
            Assert.AreEqual(expected, actual, "GetId did not retrieve the assigned value from the class successfully.");
        }
        /// <summary>
        /// Contributors the get pin code.
        /// </summary>
        [TestMethod]
        public void ContributorGetPinCode()
        {
            // set arrange variables and initialise contributor class instance
            int expected = 1431;
            SqlDateTime lastSignIn = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            Contributor contributor = new Contributor(0, "Andy", "Carroll", "Author", 1431, lastSignIn);

            // get the id
            int actual = contributor.GetPinCode();

            // compare the expected id with actual id
            Assert.AreEqual(expected, actual, "GetPinCode did not retrieve the assigned value from the class successfully.");
        }
        /// <summary>
        /// Contributors the set pin code.
        /// </summary>
        [TestMethod]
        public void ContributorSetPinCode()
        {
            // set arrange variables and initialise contributor class instance
            int expected = 1010;
            SqlDateTime lastSignIn = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            Contributor contributor = new Contributor(0, "Andy", "Carroll", "Author", 1431, lastSignIn);

            // set the actual pincode
            contributor.SetPinCode(1010);

            // compare the expected pincode with actual pincode
            int actual = contributor.GetPinCode();
            Assert.AreEqual(expected, actual, "SetPinCode did not assign a new pincode to the contributor.");
        }
        /// <summary>
        /// Contributors the get last sign in.
        /// </summary>
        [TestMethod]
        public void ContributorGetLastSignIn()
        {
            // set arrange variables and initialise contributor class instance
            SqlDateTime expected = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            SqlDateTime lastSignIn = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            Contributor contributor = new Contributor(0, "Andy", "Carroll", "Author", 1431, lastSignIn);

            // get the actual last sign in date 
            SqlDateTime actual = contributor.GetLastSignIn();

            // compare the expected id with actual id
            Assert.AreEqual(expected, actual, "GetLastSignIn did not retrieve the correct value from the class successfully.");
        }
        /// <summary>
        /// Contributors the set last sign in.
        /// </summary>
        [TestMethod]
        public void ContributorSetLastSignIn()
        {
            // set arrange variables and initialise contributor class instance
            SqlDateTime expected = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            SqlDateTime lastSignIn = new SqlDateTime(DateTime.Parse("1900-01-01 00:00:00.000"));
            Contributor contributor = new Contributor(0, "Andy", "Carroll", "Author", 1431, lastSignIn);

            // get the actual last sign in date 
            contributor.SetLastSignIn(expected);

            // compare the expected id with actual id
            SqlDateTime actual = contributor.GetLastSignIn();
            Assert.AreEqual(expected, actual, "GetLastSignIn did not retrieve the correct value from the class successfully.");
        }
    }
}
