﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Net;

namespace BugTrackerTests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class VersionControlTest
    {
        /// <summary>
        /// Tests the github source code connection_ incorrect syntax.
        /// </summary>
        [TestMethod]
        public void VersionControlGetFileContents_TestOffline()
        {
            Dictionary<string,string> commits = VersionControl.retrieveAllGithubCommits();

            // error was caught by wrapping HttpWebResponse in a try and catch statement
            Assert.IsNotNull(commits);
        }

        /// <summary>
        /// Tests the github projects connection_ broken URL.
        /// </summary>
        [TestMethod]
        public void VersionControlRetrieveCommits_RetrievesCommits()
        {
            // create the code for retrieving commits
            /*
            string url = api_url + userName + "/" + repository + "/commits";
            IList<JToken> results;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "GET";
            request.ContentType = "application/json";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            var encoding = Encoding.ASCII;
            using (var reader = new StreamReader(response.GetResponseStream(), encoding))
            {
                JObject versions = JObject.Parse(reader.ReadToEnd());
                results = versions["values"].Children().ToList();
            }
            */

            // we request from bitbucket a list of commits
            Dictionary<string,string> commits = VersionControl.retrieveAllGithubCommits();

            // set the expected length (act)
            int ExpectedLength = 1;
            bool commitsRetrieved;

            // if the server has retrieved commits the commits variable should have a length of 1 or more
            if( commits.Count >= ExpectedLength)
            {
                commitsRetrieved = true;
            } else
            {
                commitsRetrieved = false;
            }

            // if the commitsRetrieved variable is true 
            Assert.IsTrue(commitsRetrieved, "The request did not retrieve the commits from Github.");
        }
        /// <summary>
        /// Tests the github source files connection_ broken URL.
        /// </summary>
        [TestMethod]
        public void VersionControlRetrieveGithubProjects_RetrieveProjects()
        {
            // we request from bitbucket a list of commits
            IList<JToken> projects = VersionControl.retrieveGithubProjects();

            // set the expected length (act)
            int ExpectedLength = 1;
            bool projectsRetrieved;

            // if the server has retrieved a list of repos (projects) the projects variable should have a length of 1 or more
            if (projects.Count >= ExpectedLength)
            {
                projectsRetrieved = true;
            }
            else
            {
                projectsRetrieved = false;
            }

            // if the projectsRetrieved variable is true 
            Assert.IsTrue(projectsRetrieved, "The request did not retrieve a list of projects from Github.");
        }
    }
}
