﻿namespace BugTracker
{
    partial class BugTracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BugTracker));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.loginErrorText = new System.Windows.Forms.Label();
            this.logoBox = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.loginPinCode = new System.Windows.Forms.TextBox();
            this.loginSubmit = new System.Windows.Forms.Button();
            this.loginFirstName = new System.Windows.Forms.TextBox();
            this.gg = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bugsPanel = new System.Windows.Forms.Panel();
            this.BugsGridView = new System.Windows.Forms.DataGridView();
            this.BugViewId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BugName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VersionControlHash = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodeAuthor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ViewBugHistory = new System.Windows.Forms.DataGridViewButtonColumn();
            this.RemoveBugBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label11 = new System.Windows.Forms.Label();
            this.editBugPanel = new System.Windows.Forms.Panel();
            this.bugEditingPanelDescriptionTextbox = new System.Windows.Forms.TextBox();
            this.bugEditingPanelSourceCodeViewer = new System.Windows.Forms.WebBrowser();
            this.addNewAuditButton = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.updateBugDescriptionButton = new System.Windows.Forms.Button();
            this.AuditHistoryDataGridView = new System.Windows.Forms.DataGridView();
            this.AuditStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuditProject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuditClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuditSourceFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuditMethod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuditLines = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugEditingPanelDescription = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.bugEditingPanelStatusLabel = new System.Windows.Forms.Label();
            this.bugEditingPanelTitle = new System.Windows.Forms.Label();
            this.addNewAuditPanel = new System.Windows.Forms.Panel();
            this.addAuditVersionControlHash = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.addAuditContributor = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.addAuditStatus = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.addAuditButton = new System.Windows.Forms.Button();
            this.addAuditSourceCode = new System.Windows.Forms.TextBox();
            this.addAuditSourceFile = new System.Windows.Forms.TextBox();
            this.addAuditEndLine = new System.Windows.Forms.TextBox();
            this.addAuditStartLine = new System.Windows.Forms.TextBox();
            this.addAuditMethod = new System.Windows.Forms.TextBox();
            this.addAuditClass = new System.Windows.Forms.TextBox();
            this.addAuditProject = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.addNewBugPanel = new System.Windows.Forms.Panel();
            this.addNewBugEndLine = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.addNewBugStartLine = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.addNewBugSourceFile = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.addNewBugName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.addNewBugVCHashBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addNewBugContributor = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.addNewBugSubmit = new System.Windows.Forms.Button();
            this.addNewBugSourceCode = new System.Windows.Forms.TextBox();
            this.addNewBugMethod = new System.Windows.Forms.TextBox();
            this.addNewBugClass = new System.Windows.Forms.TextBox();
            this.addNewBugProject = new System.Windows.Forms.TextBox();
            this.addBugViaGithubPanel = new System.Windows.Forms.Panel();
            this.addBugUsingGithubCommit = new System.Windows.Forms.ComboBox();
            this.addBugUsingGithubName = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.addBugUsingGithubContributor = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.addBugUsingGithubSourceFile = new System.Windows.Forms.ComboBox();
            this.addBugUsingGithubProject = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.addBugUsingGithubSourceCode = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.addBugUsingGithubEndLine = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.addBugUsingGithubStartLine = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.addBugUsingGithubClass = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.addBugUsingGithubMethod = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.addBugUsingGithubSubmitButton = new System.Windows.Forms.Button();
            this.addBugUsingGithubPreview = new System.Windows.Forms.WebBrowser();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.logOutButton = new System.Windows.Forms.Button();
            this.contributorInfoLabel = new System.Windows.Forms.Label();
            this.lastSignInDate = new System.Windows.Forms.Label();
            this.logoBoxMain = new System.Windows.Forms.PictureBox();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.bugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportANewBugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportBugViaGithubToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label42 = new System.Windows.Forms.Label();
            this.addAuditCancelButton = new System.Windows.Forms.Button();
            this.loginPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).BeginInit();
            this.panel2.SuspendLayout();
            this.bugsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BugsGridView)).BeginInit();
            this.editBugPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AuditHistoryDataGridView)).BeginInit();
            this.addNewAuditPanel.SuspendLayout();
            this.addNewBugPanel.SuspendLayout();
            this.addBugViaGithubPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBoxMain)).BeginInit();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // loginPanel
            // 
            this.loginPanel.BackColor = System.Drawing.SystemColors.HotTrack;
            this.loginPanel.Controls.Add(this.loginErrorText);
            this.loginPanel.Controls.Add(this.logoBox);
            this.loginPanel.Controls.Add(this.label3);
            this.loginPanel.Controls.Add(this.label2);
            this.loginPanel.Controls.Add(this.label1);
            this.loginPanel.Controls.Add(this.loginPinCode);
            this.loginPanel.Controls.Add(this.loginSubmit);
            this.loginPanel.Controls.Add(this.loginFirstName);
            this.loginPanel.Controls.Add(this.gg);
            this.loginPanel.Location = new System.Drawing.Point(2000, 2000);
            this.loginPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(1671, 974);
            this.loginPanel.TabIndex = 0;
            // 
            // loginErrorText
            // 
            this.loginErrorText.AutoSize = true;
            this.loginErrorText.BackColor = System.Drawing.SystemColors.ControlText;
            this.loginErrorText.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginErrorText.ForeColor = System.Drawing.Color.Red;
            this.loginErrorText.Location = new System.Drawing.Point(451, 728);
            this.loginErrorText.Name = "loginErrorText";
            this.loginErrorText.Size = new System.Drawing.Size(228, 32);
            this.loginErrorText.TabIndex = 8;
            this.loginErrorText.Text = "No Error Currently";
            this.loginErrorText.Visible = false;
            // 
            // logoBox
            // 
            this.logoBox.Image = global::BugTracker.Properties.Resources.logo;
            this.logoBox.InitialImage = ((System.Drawing.Image)(resources.GetObject("logoBox.InitialImage")));
            this.logoBox.Location = new System.Drawing.Point(636, 249);
            this.logoBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.logoBox.Name = "logoBox";
            this.logoBox.Size = new System.Drawing.Size(348, 158);
            this.logoBox.TabIndex = 7;
            this.logoBox.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(817, 557);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 31);
            this.label3.TabIndex = 6;
            this.label3.Text = "4 Digit Pincode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(445, 556);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 31);
            this.label2.TabIndex = 5;
            this.label2.Text = "Username";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(616, 474);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(368, 28);
            this.label1.TabIndex = 4;
            this.label1.Text = "Please enter your name and account pin. ";
            // 
            // loginPinCode
            // 
            this.loginPinCode.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginPinCode.Location = new System.Drawing.Point(824, 597);
            this.loginPinCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loginPinCode.Name = "loginPinCode";
            this.loginPinCode.Size = new System.Drawing.Size(344, 37);
            this.loginPinCode.TabIndex = 2;
            // 
            // loginSubmit
            // 
            this.loginSubmit.BackColor = System.Drawing.SystemColors.Highlight;
            this.loginSubmit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loginSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginSubmit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginSubmit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loginSubmit.Location = new System.Drawing.Point(451, 654);
            this.loginSubmit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loginSubmit.Name = "loginSubmit";
            this.loginSubmit.Size = new System.Drawing.Size(717, 53);
            this.loginSubmit.TabIndex = 3;
            this.loginSubmit.Text = "LOGIN";
            this.loginSubmit.UseVisualStyleBackColor = false;
            this.loginSubmit.Click += new System.EventHandler(this.button1_Click);
            // 
            // loginFirstName
            // 
            this.loginFirstName.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginFirstName.Location = new System.Drawing.Point(451, 597);
            this.loginFirstName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loginFirstName.Name = "loginFirstName";
            this.loginFirstName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.loginFirstName.Size = new System.Drawing.Size(342, 37);
            this.loginFirstName.TabIndex = 1;
            // 
            // gg
            // 
            this.gg.AutoSize = true;
            this.gg.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gg.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gg.Location = new System.Drawing.Point(672, 432);
            this.gg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gg.Name = "gg";
            this.gg.Size = new System.Drawing.Size(259, 32);
            this.gg.TabIndex = 0;
            this.gg.Text = "Login To Your Account";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel2.Controls.Add(this.editBugPanel);
            this.panel2.Controls.Add(this.addNewAuditPanel);
            this.panel2.Controls.Add(this.bugsPanel);
            this.panel2.Controls.Add(this.addNewBugPanel);
            this.panel2.Controls.Add(this.addBugViaGithubPanel);
            this.panel2.Controls.Add(this.logOutButton);
            this.panel2.Controls.Add(this.contributorInfoLabel);
            this.panel2.Controls.Add(this.lastSignInDate);
            this.panel2.Controls.Add(this.logoBoxMain);
            this.panel2.Controls.Add(this.mainMenu);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1671, 975);
            this.panel2.TabIndex = 1;
            // 
            // bugsPanel
            // 
            this.bugsPanel.BackColor = System.Drawing.SystemColors.Window;
            this.bugsPanel.Controls.Add(this.BugsGridView);
            this.bugsPanel.Controls.Add(this.label11);
            this.bugsPanel.Location = new System.Drawing.Point(300, 0);
            this.bugsPanel.Name = "bugsPanel";
            this.bugsPanel.Size = new System.Drawing.Size(1321, 975);
            this.bugsPanel.TabIndex = 8;
            this.bugsPanel.Visible = false;
            // 
            // BugsGridView
            // 
            this.BugsGridView.AllowUserToAddRows = false;
            this.BugsGridView.AllowUserToDeleteRows = false;
            this.BugsGridView.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.BugsGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BugsGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.BugsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BugsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BugViewId,
            this.BugName,
            this.VersionControlHash,
            this.CodeAuthor,
            this.Status,
            this.ViewBugHistory,
            this.RemoveBugBtn});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.BugsGridView.DefaultCellStyle = dataGridViewCellStyle10;
            this.BugsGridView.Location = new System.Drawing.Point(26, 142);
            this.BugsGridView.Name = "BugsGridView";
            this.BugsGridView.ReadOnly = true;
            this.BugsGridView.RowTemplate.Height = 28;
            this.BugsGridView.Size = new System.Drawing.Size(1250, 812);
            this.BugsGridView.TabIndex = 1;
            this.BugsGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.BugsGridView_CellContentClick);
            // 
            // BugViewId
            // 
            this.BugViewId.HeaderText = "BugId";
            this.BugViewId.Name = "BugViewId";
            this.BugViewId.ReadOnly = true;
            this.BugViewId.Visible = false;
            this.BugViewId.Width = 120;
            // 
            // BugName
            // 
            this.BugName.HeaderText = "Description of the Bug";
            this.BugName.Name = "BugName";
            this.BugName.ReadOnly = true;
            this.BugName.Width = 300;
            // 
            // VersionControlHash
            // 
            this.VersionControlHash.HeaderText = "VC Hash";
            this.VersionControlHash.Name = "VersionControlHash";
            this.VersionControlHash.ReadOnly = true;
            // 
            // CodeAuthor
            // 
            this.CodeAuthor.HeaderText = "Code Author ";
            this.CodeAuthor.Name = "CodeAuthor";
            this.CodeAuthor.ReadOnly = true;
            this.CodeAuthor.Width = 130;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // ViewBugHistory
            // 
            this.ViewBugHistory.HeaderText = "";
            this.ViewBugHistory.Name = "ViewBugHistory";
            this.ViewBugHistory.ReadOnly = true;
            // 
            // RemoveBugBtn
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Red;
            this.RemoveBugBtn.DefaultCellStyle = dataGridViewCellStyle9;
            this.RemoveBugBtn.HeaderText = "";
            this.RemoveBugBtn.Name = "RemoveBugBtn";
            this.RemoveBugBtn.ReadOnly = true;
            this.RemoveBugBtn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RemoveBugBtn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(19, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(315, 48);
            this.label11.TabIndex = 0;
            this.label11.Text = "Outstanding Bugs";
            // 
            // editBugPanel
            // 
            this.editBugPanel.BackColor = System.Drawing.SystemColors.Window;
            this.editBugPanel.Controls.Add(this.label42);
            this.editBugPanel.Controls.Add(this.bugEditingPanelDescriptionTextbox);
            this.editBugPanel.Controls.Add(this.bugEditingPanelSourceCodeViewer);
            this.editBugPanel.Controls.Add(this.addNewAuditButton);
            this.editBugPanel.Controls.Add(this.label18);
            this.editBugPanel.Controls.Add(this.updateBugDescriptionButton);
            this.editBugPanel.Controls.Add(this.AuditHistoryDataGridView);
            this.editBugPanel.Controls.Add(this.bugEditingPanelDescription);
            this.editBugPanel.Controls.Add(this.label16);
            this.editBugPanel.Controls.Add(this.bugEditingPanelStatusLabel);
            this.editBugPanel.Controls.Add(this.bugEditingPanelTitle);
            this.editBugPanel.Location = new System.Drawing.Point(300, 0);
            this.editBugPanel.Name = "editBugPanel";
            this.editBugPanel.Size = new System.Drawing.Size(1325, 975);
            this.editBugPanel.TabIndex = 9;
            this.editBugPanel.Visible = false;
            // 
            // bugEditingPanelDescriptionTextbox
            // 
            this.bugEditingPanelDescriptionTextbox.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugEditingPanelDescriptionTextbox.Location = new System.Drawing.Point(36, 173);
            this.bugEditingPanelDescriptionTextbox.Multiline = true;
            this.bugEditingPanelDescriptionTextbox.Name = "bugEditingPanelDescriptionTextbox";
            this.bugEditingPanelDescriptionTextbox.Size = new System.Drawing.Size(472, 160);
            this.bugEditingPanelDescriptionTextbox.TabIndex = 9;
            this.bugEditingPanelDescriptionTextbox.Visible = false;
            // 
            // bugEditingPanelSourceCodeViewer
            // 
            this.bugEditingPanelSourceCodeViewer.Location = new System.Drawing.Point(551, 171);
            this.bugEditingPanelSourceCodeViewer.MinimumSize = new System.Drawing.Size(20, 20);
            this.bugEditingPanelSourceCodeViewer.Name = "bugEditingPanelSourceCodeViewer";
            this.bugEditingPanelSourceCodeViewer.Size = new System.Drawing.Size(715, 161);
            this.bugEditingPanelSourceCodeViewer.TabIndex = 8;
            // 
            // addNewAuditButton
            // 
            this.addNewAuditButton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.addNewAuditButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addNewAuditButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.addNewAuditButton.ForeColor = System.Drawing.SystemColors.Window;
            this.addNewAuditButton.Location = new System.Drawing.Point(36, 913);
            this.addNewAuditButton.Name = "addNewAuditButton";
            this.addNewAuditButton.Size = new System.Drawing.Size(256, 48);
            this.addNewAuditButton.TabIndex = 7;
            this.addNewAuditButton.Text = "Add Additional Audit ";
            this.addNewAuditButton.UseVisualStyleBackColor = false;
            this.addNewAuditButton.Click += new System.EventHandler(this.addNewAuditButton_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(30, 411);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(214, 45);
            this.label18.TabIndex = 6;
            this.label18.Text = "Audit History";
            // 
            // updateBugDescriptionButton
            // 
            this.updateBugDescriptionButton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.updateBugDescriptionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateBugDescriptionButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.updateBugDescriptionButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.updateBugDescriptionButton.Location = new System.Drawing.Point(35, 347);
            this.updateBugDescriptionButton.Name = "updateBugDescriptionButton";
            this.updateBugDescriptionButton.Size = new System.Drawing.Size(257, 45);
            this.updateBugDescriptionButton.TabIndex = 5;
            this.updateBugDescriptionButton.Text = "Update Bug Description";
            this.updateBugDescriptionButton.UseVisualStyleBackColor = false;
            this.updateBugDescriptionButton.Click += new System.EventHandler(this.updateBugDescriptionButton_Click);
            // 
            // AuditHistoryDataGridView
            // 
            this.AuditHistoryDataGridView.AllowUserToAddRows = false;
            this.AuditHistoryDataGridView.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.AuditHistoryDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(0, 20, 0, 20);
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AuditHistoryDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.AuditHistoryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AuditHistoryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AuditStatus,
            this.AuditProject,
            this.AuditClass,
            this.AuditSourceFile,
            this.AuditMethod,
            this.AuditLines});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AuditHistoryDataGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.AuditHistoryDataGridView.Location = new System.Drawing.Point(36, 481);
            this.AuditHistoryDataGridView.Name = "AuditHistoryDataGridView";
            this.AuditHistoryDataGridView.ReadOnly = true;
            this.AuditHistoryDataGridView.RowTemplate.Height = 28;
            this.AuditHistoryDataGridView.Size = new System.Drawing.Size(1230, 411);
            this.AuditHistoryDataGridView.TabIndex = 4;
            // 
            // AuditStatus
            // 
            this.AuditStatus.HeaderText = "Status";
            this.AuditStatus.Name = "AuditStatus";
            this.AuditStatus.ReadOnly = true;
            this.AuditStatus.Width = 140;
            // 
            // AuditProject
            // 
            this.AuditProject.HeaderText = "Project";
            this.AuditProject.Name = "AuditProject";
            this.AuditProject.ReadOnly = true;
            this.AuditProject.Width = 140;
            // 
            // AuditClass
            // 
            this.AuditClass.HeaderText = "Class";
            this.AuditClass.Name = "AuditClass";
            this.AuditClass.ReadOnly = true;
            this.AuditClass.Width = 140;
            // 
            // AuditSourceFile
            // 
            this.AuditSourceFile.HeaderText = "Source File";
            this.AuditSourceFile.Name = "AuditSourceFile";
            this.AuditSourceFile.ReadOnly = true;
            this.AuditSourceFile.Width = 140;
            // 
            // AuditMethod
            // 
            this.AuditMethod.HeaderText = "Method";
            this.AuditMethod.Name = "AuditMethod";
            this.AuditMethod.ReadOnly = true;
            this.AuditMethod.Width = 140;
            // 
            // AuditLines
            // 
            this.AuditLines.HeaderText = "Lines";
            this.AuditLines.Name = "AuditLines";
            this.AuditLines.ReadOnly = true;
            this.AuditLines.Width = 118;
            // 
            // bugEditingPanelDescription
            // 
            this.bugEditingPanelDescription.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugEditingPanelDescription.Location = new System.Drawing.Point(30, 167);
            this.bugEditingPanelDescription.Name = "bugEditingPanelDescription";
            this.bugEditingPanelDescription.Size = new System.Drawing.Size(478, 170);
            this.bugEditingPanelDescription.TabIndex = 3;
            this.bugEditingPanelDescription.Text = "Failed to connect to SQL Server Database.\r\nAdded connection string to the Visual " +
    "Studio solution.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(30, 134);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(157, 28);
            this.label16.TabIndex = 2;
            this.label16.Text = "Bug Description";
            // 
            // bugEditingPanelStatusLabel
            // 
            this.bugEditingPanelStatusLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bugEditingPanelStatusLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugEditingPanelStatusLabel.Location = new System.Drawing.Point(571, 47);
            this.bugEditingPanelStatusLabel.Name = "bugEditingPanelStatusLabel";
            this.bugEditingPanelStatusLabel.Size = new System.Drawing.Size(703, 28);
            this.bugEditingPanelStatusLabel.TabIndex = 1;
            this.bugEditingPanelStatusLabel.Text = "CURRENT STATUS: UNCONFIRMED";
            this.bugEditingPanelStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bugEditingPanelTitle
            // 
            this.bugEditingPanelTitle.AutoSize = true;
            this.bugEditingPanelTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugEditingPanelTitle.Location = new System.Drawing.Point(28, 32);
            this.bugEditingPanelTitle.Name = "bugEditingPanelTitle";
            this.bugEditingPanelTitle.Size = new System.Drawing.Size(158, 48);
            this.bugEditingPanelTitle.TabIndex = 0;
            this.bugEditingPanelTitle.Text = "Edit Bug";
            // 
            // addNewAuditPanel
            // 
            this.addNewAuditPanel.BackColor = System.Drawing.SystemColors.Window;
            this.addNewAuditPanel.Controls.Add(this.addAuditCancelButton);
            this.addNewAuditPanel.Controls.Add(this.addAuditVersionControlHash);
            this.addNewAuditPanel.Controls.Add(this.label28);
            this.addNewAuditPanel.Controls.Add(this.addAuditContributor);
            this.addNewAuditPanel.Controls.Add(this.label27);
            this.addNewAuditPanel.Controls.Add(this.addAuditStatus);
            this.addNewAuditPanel.Controls.Add(this.label26);
            this.addNewAuditPanel.Controls.Add(this.label25);
            this.addNewAuditPanel.Controls.Add(this.label24);
            this.addNewAuditPanel.Controls.Add(this.label23);
            this.addNewAuditPanel.Controls.Add(this.label22);
            this.addNewAuditPanel.Controls.Add(this.label21);
            this.addNewAuditPanel.Controls.Add(this.label20);
            this.addNewAuditPanel.Controls.Add(this.label19);
            this.addNewAuditPanel.Controls.Add(this.addAuditButton);
            this.addNewAuditPanel.Controls.Add(this.addAuditSourceCode);
            this.addNewAuditPanel.Controls.Add(this.addAuditSourceFile);
            this.addNewAuditPanel.Controls.Add(this.addAuditEndLine);
            this.addNewAuditPanel.Controls.Add(this.addAuditStartLine);
            this.addNewAuditPanel.Controls.Add(this.addAuditMethod);
            this.addNewAuditPanel.Controls.Add(this.addAuditClass);
            this.addNewAuditPanel.Controls.Add(this.addAuditProject);
            this.addNewAuditPanel.Controls.Add(this.label13);
            this.addNewAuditPanel.Location = new System.Drawing.Point(300, 0);
            this.addNewAuditPanel.Name = "addNewAuditPanel";
            this.addNewAuditPanel.Size = new System.Drawing.Size(1322, 975);
            this.addNewAuditPanel.TabIndex = 12;
            // 
            // addAuditVersionControlHash
            // 
            this.addAuditVersionControlHash.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditVersionControlHash.Location = new System.Drawing.Point(33, 442);
            this.addAuditVersionControlHash.Name = "addAuditVersionControlHash";
            this.addAuditVersionControlHash.Size = new System.Drawing.Size(1232, 31);
            this.addAuditVersionControlHash.TabIndex = 21;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(29, 409);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(205, 28);
            this.label28.TabIndex = 20;
            this.label28.Text = "Version Control Hash";
            // 
            // addAuditContributor
            // 
            this.addAuditContributor.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditContributor.FormattingEnabled = true;
            this.addAuditContributor.Location = new System.Drawing.Point(33, 854);
            this.addAuditContributor.Name = "addAuditContributor";
            this.addAuditContributor.Size = new System.Drawing.Size(1232, 33);
            this.addAuditContributor.TabIndex = 19;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(29, 821);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(128, 28);
            this.label27.TabIndex = 18;
            this.label27.Text = "Code Author";
            // 
            // addAuditStatus
            // 
            this.addAuditStatus.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditStatus.FormattingEnabled = true;
            this.addAuditStatus.Location = new System.Drawing.Point(35, 590);
            this.addAuditStatus.Name = "addAuditStatus";
            this.addAuditStatus.Size = new System.Drawing.Size(1226, 33);
            this.addAuditStatus.TabIndex = 17;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(30, 557);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 28);
            this.label26.TabIndex = 16;
            this.label26.Text = "Status";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(29, 633);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(127, 28);
            this.label25.TabIndex = 15;
            this.label25.Text = "Source Code";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(30, 480);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(111, 28);
            this.label24.TabIndex = 14;
            this.label24.Text = "Source File";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(594, 336);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(90, 28);
            this.label23.TabIndex = 13;
            this.label23.Text = "End Line";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(29, 337);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(98, 28);
            this.label22.TabIndex = 12;
            this.label22.Text = "Start Line";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(29, 261);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 28);
            this.label21.TabIndex = 11;
            this.label21.Text = "Method";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(29, 181);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 28);
            this.label20.TabIndex = 10;
            this.label20.Text = "Class";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(31, 104);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 28);
            this.label19.TabIndex = 9;
            this.label19.Text = "Project";
            // 
            // addAuditButton
            // 
            this.addAuditButton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.addAuditButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addAuditButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.addAuditButton.ForeColor = System.Drawing.SystemColors.Window;
            this.addAuditButton.Location = new System.Drawing.Point(33, 908);
            this.addAuditButton.Name = "addAuditButton";
            this.addAuditButton.Size = new System.Drawing.Size(166, 40);
            this.addAuditButton.TabIndex = 8;
            this.addAuditButton.Text = "Add Audit";
            this.addAuditButton.UseVisualStyleBackColor = false;
            this.addAuditButton.Click += new System.EventHandler(this.addAuditButton_Click);
            // 
            // addAuditSourceCode
            // 
            this.addAuditSourceCode.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditSourceCode.Location = new System.Drawing.Point(33, 666);
            this.addAuditSourceCode.Multiline = true;
            this.addAuditSourceCode.Name = "addAuditSourceCode";
            this.addAuditSourceCode.Size = new System.Drawing.Size(1230, 146);
            this.addAuditSourceCode.TabIndex = 7;
            // 
            // addAuditSourceFile
            // 
            this.addAuditSourceFile.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditSourceFile.Location = new System.Drawing.Point(35, 513);
            this.addAuditSourceFile.Name = "addAuditSourceFile";
            this.addAuditSourceFile.Size = new System.Drawing.Size(1230, 31);
            this.addAuditSourceFile.TabIndex = 6;
            // 
            // addAuditEndLine
            // 
            this.addAuditEndLine.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditEndLine.Location = new System.Drawing.Point(600, 369);
            this.addAuditEndLine.Name = "addAuditEndLine";
            this.addAuditEndLine.Size = new System.Drawing.Size(666, 31);
            this.addAuditEndLine.TabIndex = 5;
            // 
            // addAuditStartLine
            // 
            this.addAuditStartLine.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditStartLine.Location = new System.Drawing.Point(35, 370);
            this.addAuditStartLine.Name = "addAuditStartLine";
            this.addAuditStartLine.Size = new System.Drawing.Size(535, 31);
            this.addAuditStartLine.TabIndex = 4;
            // 
            // addAuditMethod
            // 
            this.addAuditMethod.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditMethod.Location = new System.Drawing.Point(35, 294);
            this.addAuditMethod.Name = "addAuditMethod";
            this.addAuditMethod.Size = new System.Drawing.Size(1230, 31);
            this.addAuditMethod.TabIndex = 3;
            // 
            // addAuditClass
            // 
            this.addAuditClass.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditClass.Location = new System.Drawing.Point(35, 214);
            this.addAuditClass.Name = "addAuditClass";
            this.addAuditClass.Size = new System.Drawing.Size(1230, 31);
            this.addAuditClass.TabIndex = 2;
            // 
            // addAuditProject
            // 
            this.addAuditProject.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.addAuditProject.Location = new System.Drawing.Point(37, 137);
            this.addAuditProject.Name = "addAuditProject";
            this.addAuditProject.Size = new System.Drawing.Size(1228, 31);
            this.addAuditProject.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(31, 37);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(550, 48);
            this.label13.TabIndex = 0;
            this.label13.Text = "Add An Additional Audit History";
            // 
            // addNewBugPanel
            // 
            this.addNewBugPanel.BackColor = System.Drawing.SystemColors.HighlightText;
            this.addNewBugPanel.Controls.Add(this.addNewBugEndLine);
            this.addNewBugPanel.Controls.Add(this.label17);
            this.addNewBugPanel.Controls.Add(this.addNewBugStartLine);
            this.addNewBugPanel.Controls.Add(this.label15);
            this.addNewBugPanel.Controls.Add(this.addNewBugSourceFile);
            this.addNewBugPanel.Controls.Add(this.label14);
            this.addNewBugPanel.Controls.Add(this.label12);
            this.addNewBugPanel.Controls.Add(this.addNewBugName);
            this.addNewBugPanel.Controls.Add(this.label10);
            this.addNewBugPanel.Controls.Add(this.label9);
            this.addNewBugPanel.Controls.Add(this.label8);
            this.addNewBugPanel.Controls.Add(this.addNewBugVCHashBox);
            this.addNewBugPanel.Controls.Add(this.label7);
            this.addNewBugPanel.Controls.Add(this.addNewBugContributor);
            this.addNewBugPanel.Controls.Add(this.label6);
            this.addNewBugPanel.Controls.Add(this.label5);
            this.addNewBugPanel.Controls.Add(this.label4);
            this.addNewBugPanel.Controls.Add(this.addNewBugSubmit);
            this.addNewBugPanel.Controls.Add(this.addNewBugSourceCode);
            this.addNewBugPanel.Controls.Add(this.addNewBugMethod);
            this.addNewBugPanel.Controls.Add(this.addNewBugClass);
            this.addNewBugPanel.Controls.Add(this.addNewBugProject);
            this.addNewBugPanel.Location = new System.Drawing.Point(300, 0);
            this.addNewBugPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addNewBugPanel.Name = "addNewBugPanel";
            this.addNewBugPanel.Size = new System.Drawing.Size(1300, 975);
            this.addNewBugPanel.TabIndex = 6;
            // 
            // addNewBugEndLine
            // 
            this.addNewBugEndLine.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugEndLine.Location = new System.Drawing.Point(304, 367);
            this.addNewBugEndLine.Name = "addNewBugEndLine";
            this.addNewBugEndLine.Size = new System.Drawing.Size(275, 34);
            this.addNewBugEndLine.TabIndex = 23;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(298, 333);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(119, 28);
            this.label17.TabIndex = 22;
            this.label17.Text = "Ending Line";
            // 
            // addNewBugStartLine
            // 
            this.addNewBugStartLine.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugStartLine.Location = new System.Drawing.Point(30, 367);
            this.addNewBugStartLine.Name = "addNewBugStartLine";
            this.addNewBugStartLine.Size = new System.Drawing.Size(247, 34);
            this.addNewBugStartLine.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(26, 333);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 28);
            this.label15.TabIndex = 20;
            this.label15.Text = "Starting Line";
            // 
            // addNewBugSourceFile
            // 
            this.addNewBugSourceFile.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugSourceFile.Location = new System.Drawing.Point(30, 657);
            this.addNewBugSourceFile.Name = "addNewBugSourceFile";
            this.addNewBugSourceFile.Size = new System.Drawing.Size(1242, 34);
            this.addNewBugSourceFile.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(26, 624);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(111, 28);
            this.label14.TabIndex = 18;
            this.label14.Text = "Source File";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(20, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(319, 48);
            this.label12.TabIndex = 17;
            this.label12.Text = "Report a New Bug";
            // 
            // addNewBugName
            // 
            this.addNewBugName.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugName.Location = new System.Drawing.Point(30, 172);
            this.addNewBugName.Name = "addNewBugName";
            this.addNewBugName.Size = new System.Drawing.Size(1242, 34);
            this.addNewBugName.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(24, 139);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(521, 28);
            this.label10.TabIndex = 15;
            this.label10.Text = "Bug Name - Type a description of the bug in question...";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(26, 709);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 28);
            this.label9.TabIndex = 14;
            this.label9.Text = "Source Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(26, 530);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(205, 28);
            this.label8.TabIndex = 13;
            this.label8.Text = "Version Control Hash";
            // 
            // addNewBugVCHashBox
            // 
            this.addNewBugVCHashBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugVCHashBox.FormattingEnabled = true;
            this.addNewBugVCHashBox.Location = new System.Drawing.Point(30, 563);
            this.addNewBugVCHashBox.Name = "addNewBugVCHashBox";
            this.addNewBugVCHashBox.Size = new System.Drawing.Size(1242, 36);
            this.addNewBugVCHashBox.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(25, 427);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 28);
            this.label7.TabIndex = 11;
            this.label7.Text = "Code Author";
            // 
            // addNewBugContributor
            // 
            this.addNewBugContributor.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugContributor.FormattingEnabled = true;
            this.addNewBugContributor.Location = new System.Drawing.Point(30, 460);
            this.addNewBugContributor.Name = "addNewBugContributor";
            this.addNewBugContributor.Size = new System.Drawing.Size(1242, 36);
            this.addNewBugContributor.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(600, 333);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 28);
            this.label6.TabIndex = 9;
            this.label6.Text = "Method Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(602, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 28);
            this.label5.TabIndex = 8;
            this.label5.Text = "Class Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(24, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 28);
            this.label4.TabIndex = 7;
            this.label4.Text = "File Name";
            // 
            // addNewBugSubmit
            // 
            this.addNewBugSubmit.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.addNewBugSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addNewBugSubmit.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.addNewBugSubmit.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.addNewBugSubmit.Location = new System.Drawing.Point(28, 905);
            this.addNewBugSubmit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addNewBugSubmit.Name = "addNewBugSubmit";
            this.addNewBugSubmit.Size = new System.Drawing.Size(175, 47);
            this.addNewBugSubmit.TabIndex = 5;
            this.addNewBugSubmit.Text = "Add New Bug";
            this.addNewBugSubmit.UseVisualStyleBackColor = false;
            this.addNewBugSubmit.Click += new System.EventHandler(this.addNewBugSubmit_Click);
            // 
            // addNewBugSourceCode
            // 
            this.addNewBugSourceCode.Font = new System.Drawing.Font("Courier New", 10F);
            this.addNewBugSourceCode.Location = new System.Drawing.Point(30, 742);
            this.addNewBugSourceCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addNewBugSourceCode.Multiline = true;
            this.addNewBugSourceCode.Name = "addNewBugSourceCode";
            this.addNewBugSourceCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.addNewBugSourceCode.Size = new System.Drawing.Size(1242, 139);
            this.addNewBugSourceCode.TabIndex = 4;
            // 
            // addNewBugMethod
            // 
            this.addNewBugMethod.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugMethod.Location = new System.Drawing.Point(606, 366);
            this.addNewBugMethod.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addNewBugMethod.Name = "addNewBugMethod";
            this.addNewBugMethod.Size = new System.Drawing.Size(666, 34);
            this.addNewBugMethod.TabIndex = 2;
            // 
            // addNewBugClass
            // 
            this.addNewBugClass.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugClass.Location = new System.Drawing.Point(606, 264);
            this.addNewBugClass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addNewBugClass.Name = "addNewBugClass";
            this.addNewBugClass.Size = new System.Drawing.Size(666, 34);
            this.addNewBugClass.TabIndex = 1;
            // 
            // addNewBugProject
            // 
            this.addNewBugProject.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addNewBugProject.Location = new System.Drawing.Point(30, 264);
            this.addNewBugProject.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addNewBugProject.Name = "addNewBugProject";
            this.addNewBugProject.Size = new System.Drawing.Size(547, 34);
            this.addNewBugProject.TabIndex = 0;
            // 
            // addBugViaGithubPanel
            // 
            this.addBugViaGithubPanel.BackColor = System.Drawing.SystemColors.Window;
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubCommit);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubName);
            this.addBugViaGithubPanel.Controls.Add(this.label41);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubContributor);
            this.addBugViaGithubPanel.Controls.Add(this.label40);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubSourceFile);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubProject);
            this.addBugViaGithubPanel.Controls.Add(this.label39);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubSourceCode);
            this.addBugViaGithubPanel.Controls.Add(this.label38);
            this.addBugViaGithubPanel.Controls.Add(this.label37);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubEndLine);
            this.addBugViaGithubPanel.Controls.Add(this.label36);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubStartLine);
            this.addBugViaGithubPanel.Controls.Add(this.label35);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubClass);
            this.addBugViaGithubPanel.Controls.Add(this.label34);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubMethod);
            this.addBugViaGithubPanel.Controls.Add(this.label33);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubSubmitButton);
            this.addBugViaGithubPanel.Controls.Add(this.addBugUsingGithubPreview);
            this.addBugViaGithubPanel.Controls.Add(this.label32);
            this.addBugViaGithubPanel.Controls.Add(this.label31);
            this.addBugViaGithubPanel.Controls.Add(this.label30);
            this.addBugViaGithubPanel.Controls.Add(this.label29);
            this.addBugViaGithubPanel.Location = new System.Drawing.Point(300, 0);
            this.addBugViaGithubPanel.Name = "addBugViaGithubPanel";
            this.addBugViaGithubPanel.Size = new System.Drawing.Size(1322, 975);
            this.addBugViaGithubPanel.TabIndex = 13;
            // 
            // addBugUsingGithubCommit
            // 
            this.addBugUsingGithubCommit.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBugUsingGithubCommit.FormattingEnabled = true;
            this.addBugUsingGithubCommit.Location = new System.Drawing.Point(24, 380);
            this.addBugUsingGithubCommit.Name = "addBugUsingGithubCommit";
            this.addBugUsingGithubCommit.Size = new System.Drawing.Size(672, 36);
            this.addBugUsingGithubCommit.TabIndex = 27;
            // 
            // addBugUsingGithubName
            // 
            this.addBugUsingGithubName.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubName.Location = new System.Drawing.Point(24, 119);
            this.addBugUsingGithubName.Name = "addBugUsingGithubName";
            this.addBugUsingGithubName.Size = new System.Drawing.Size(1237, 34);
            this.addBugUsingGithubName.TabIndex = 26;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label41.Location = new System.Drawing.Point(17, 86);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(157, 28);
            this.label41.TabIndex = 25;
            this.label41.Text = "Bug Description";
            // 
            // addBugUsingGithubContributor
            // 
            this.addBugUsingGithubContributor.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubContributor.FormattingEnabled = true;
            this.addBugUsingGithubContributor.Location = new System.Drawing.Point(22, 286);
            this.addBugUsingGithubContributor.Name = "addBugUsingGithubContributor";
            this.addBugUsingGithubContributor.Size = new System.Drawing.Size(674, 36);
            this.addBugUsingGithubContributor.TabIndex = 24;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label40.Location = new System.Drawing.Point(17, 253);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(128, 28);
            this.label40.TabIndex = 23;
            this.label40.Text = "Code Author";
            // 
            // addBugUsingGithubSourceFile
            // 
            this.addBugUsingGithubSourceFile.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubSourceFile.FormattingEnabled = true;
            this.addBugUsingGithubSourceFile.Location = new System.Drawing.Point(728, 196);
            this.addBugUsingGithubSourceFile.Name = "addBugUsingGithubSourceFile";
            this.addBugUsingGithubSourceFile.Size = new System.Drawing.Size(535, 36);
            this.addBugUsingGithubSourceFile.TabIndex = 22;
            this.addBugUsingGithubSourceFile.SelectionChangeCommitted += new System.EventHandler(this.addBugUsingGithubSourceFile_SelectionChangeCommitted);
            // 
            // addBugUsingGithubProject
            // 
            this.addBugUsingGithubProject.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubProject.FormattingEnabled = true;
            this.addBugUsingGithubProject.Location = new System.Drawing.Point(22, 197);
            this.addBugUsingGithubProject.Name = "addBugUsingGithubProject";
            this.addBugUsingGithubProject.Size = new System.Drawing.Size(674, 36);
            this.addBugUsingGithubProject.TabIndex = 21;
            this.addBugUsingGithubProject.SelectionChangeCommitted += new System.EventHandler(this.addBugUsingGithubProject_SelectionChangeCommitted);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(724, 633);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(127, 28);
            this.label39.TabIndex = 20;
            this.label39.Text = "Source Code";
            // 
            // addBugUsingGithubSourceCode
            // 
            this.addBugUsingGithubSourceCode.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubSourceCode.Location = new System.Drawing.Point(729, 666);
            this.addBugUsingGithubSourceCode.Multiline = true;
            this.addBugUsingGithubSourceCode.Name = "addBugUsingGithubSourceCode";
            this.addBugUsingGithubSourceCode.Size = new System.Drawing.Size(534, 277);
            this.addBugUsingGithubSourceCode.TabIndex = 19;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(19, 347);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(205, 28);
            this.label38.TabIndex = 17;
            this.label38.Text = "Version Control Hash";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(729, 644);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(0, 20);
            this.label37.TabIndex = 16;
            // 
            // addBugUsingGithubEndLine
            // 
            this.addBugUsingGithubEndLine.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubEndLine.Location = new System.Drawing.Point(729, 569);
            this.addBugUsingGithubEndLine.Name = "addBugUsingGithubEndLine";
            this.addBugUsingGithubEndLine.Size = new System.Drawing.Size(536, 34);
            this.addBugUsingGithubEndLine.TabIndex = 15;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(723, 536);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(90, 28);
            this.label36.TabIndex = 14;
            this.label36.Text = "End Line";
            // 
            // addBugUsingGithubStartLine
            // 
            this.addBugUsingGithubStartLine.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubStartLine.Location = new System.Drawing.Point(729, 474);
            this.addBugUsingGithubStartLine.Name = "addBugUsingGithubStartLine";
            this.addBugUsingGithubStartLine.Size = new System.Drawing.Size(536, 34);
            this.addBugUsingGithubStartLine.TabIndex = 13;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(724, 441);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(98, 28);
            this.label35.TabIndex = 12;
            this.label35.Text = "Start Line";
            // 
            // addBugUsingGithubClass
            // 
            this.addBugUsingGithubClass.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubClass.Location = new System.Drawing.Point(729, 381);
            this.addBugUsingGithubClass.Name = "addBugUsingGithubClass";
            this.addBugUsingGithubClass.Size = new System.Drawing.Size(536, 34);
            this.addBugUsingGithubClass.TabIndex = 11;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(724, 348);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(57, 28);
            this.label34.TabIndex = 10;
            this.label34.Text = "Class";
            // 
            // addBugUsingGithubMethod
            // 
            this.addBugUsingGithubMethod.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addBugUsingGithubMethod.Location = new System.Drawing.Point(729, 287);
            this.addBugUsingGithubMethod.Name = "addBugUsingGithubMethod";
            this.addBugUsingGithubMethod.Size = new System.Drawing.Size(534, 34);
            this.addBugUsingGithubMethod.TabIndex = 9;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(723, 254);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(84, 28);
            this.label33.TabIndex = 8;
            this.label33.Text = "Method";
            // 
            // addBugUsingGithubSubmitButton
            // 
            this.addBugUsingGithubSubmitButton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.addBugUsingGithubSubmitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addBugUsingGithubSubmitButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.addBugUsingGithubSubmitButton.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.addBugUsingGithubSubmitButton.Location = new System.Drawing.Point(27, 899);
            this.addBugUsingGithubSubmitButton.Name = "addBugUsingGithubSubmitButton";
            this.addBugUsingGithubSubmitButton.Size = new System.Drawing.Size(669, 45);
            this.addBugUsingGithubSubmitButton.TabIndex = 5;
            this.addBugUsingGithubSubmitButton.Text = "Add Bug Report";
            this.addBugUsingGithubSubmitButton.UseVisualStyleBackColor = false;
            this.addBugUsingGithubSubmitButton.Click += new System.EventHandler(this.addBugUsingGithubSubmitButton_Click);
            // 
            // addBugUsingGithubPreview
            // 
            this.addBugUsingGithubPreview.Location = new System.Drawing.Point(30, 485);
            this.addBugUsingGithubPreview.MinimumSize = new System.Drawing.Size(20, 20);
            this.addBugUsingGithubPreview.Name = "addBugUsingGithubPreview";
            this.addBugUsingGithubPreview.Size = new System.Drawing.Size(666, 378);
            this.addBugUsingGithubPreview.TabIndex = 4;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(18, 444);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(120, 28);
            this.label32.TabIndex = 3;
            this.label32.Text = "File Preview";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(723, 163);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(122, 28);
            this.label31.TabIndex = 2;
            this.label31.Text = "Select A File";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(17, 164);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(154, 28);
            this.label30.TabIndex = 1;
            this.label30.Text = "Select A Project";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(15, 26);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(279, 38);
            this.label29.TabIndex = 0;
            this.label29.Text = "Add Bug Via Github";
            // 
            // logOutButton
            // 
            this.logOutButton.BackColor = System.Drawing.SystemColors.ControlText;
            this.logOutButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.logOutButton.FlatAppearance.BorderSize = 0;
            this.logOutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logOutButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logOutButton.ForeColor = System.Drawing.SystemColors.Control;
            this.logOutButton.Location = new System.Drawing.Point(0, 862);
            this.logOutButton.Margin = new System.Windows.Forms.Padding(0);
            this.logOutButton.Name = "logOutButton";
            this.logOutButton.Size = new System.Drawing.Size(305, 39);
            this.logOutButton.TabIndex = 11;
            this.logOutButton.Text = "LOGOUT";
            this.logOutButton.UseVisualStyleBackColor = false;
            this.logOutButton.Click += new System.EventHandler(this.logOutButton_Click);
            // 
            // contributorInfoLabel
            // 
            this.contributorInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.contributorInfoLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contributorInfoLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.contributorInfoLabel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.contributorInfoLabel.Location = new System.Drawing.Point(0, 817);
            this.contributorInfoLabel.Name = "contributorInfoLabel";
            this.contributorInfoLabel.Size = new System.Drawing.Size(298, 30);
            this.contributorInfoLabel.TabIndex = 10;
            this.contributorInfoLabel.Text = "Welcome George!";
            this.contributorInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lastSignInDate
            // 
            this.lastSignInDate.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastSignInDate.ForeColor = System.Drawing.Color.White;
            this.lastSignInDate.Location = new System.Drawing.Point(3, 899);
            this.lastSignInDate.Name = "lastSignInDate";
            this.lastSignInDate.Size = new System.Drawing.Size(295, 76);
            this.lastSignInDate.TabIndex = 7;
            this.lastSignInDate.Text = "Last Signed In: \r\n29th November 2015";
            this.lastSignInDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // logoBoxMain
            // 
            this.logoBoxMain.Image = global::BugTracker.Properties.Resources.logo;
            this.logoBoxMain.Location = new System.Drawing.Point(26, 18);
            this.logoBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.logoBoxMain.Name = "logoBoxMain";
            this.logoBoxMain.Size = new System.Drawing.Size(253, 136);
            this.logoBoxMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoBoxMain.TabIndex = 0;
            this.logoBoxMain.TabStop = false;
            // 
            // mainMenu
            // 
            this.mainMenu.AutoSize = false;
            this.mainMenu.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.mainMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.mainMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bugsToolStripMenuItem,
            this.reportANewBugToolStripMenuItem,
            this.reportBugViaGithubToolStripMenuItem});
            this.mainMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.mainMenu.Location = new System.Drawing.Point(0, 172);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Padding = new System.Windows.Forms.Padding(14);
            this.mainMenu.Size = new System.Drawing.Size(305, 286);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "menuStrip1";
            // 
            // bugsToolStripMenuItem
            // 
            this.bugsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugsToolStripMenuItem.Name = "bugsToolStripMenuItem";
            this.bugsToolStripMenuItem.Padding = new System.Windows.Forms.Padding(14);
            this.bugsToolStripMenuItem.Size = new System.Drawing.Size(276, 64);
            this.bugsToolStripMenuItem.Text = "Bugs";
            this.bugsToolStripMenuItem.Click += new System.EventHandler(this.bugsToolStripMenuItem_Click);
            // 
            // reportANewBugToolStripMenuItem
            // 
            this.reportANewBugToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportANewBugToolStripMenuItem.Name = "reportANewBugToolStripMenuItem";
            this.reportANewBugToolStripMenuItem.Padding = new System.Windows.Forms.Padding(14);
            this.reportANewBugToolStripMenuItem.Size = new System.Drawing.Size(276, 64);
            this.reportANewBugToolStripMenuItem.Text = "Report A New Bug";
            this.reportANewBugToolStripMenuItem.Click += new System.EventHandler(this.reportANewBugToolStripMenuItem_Click);
            // 
            // reportBugViaGithubToolStripMenuItem
            // 
            this.reportBugViaGithubToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportBugViaGithubToolStripMenuItem.Name = "reportBugViaGithubToolStripMenuItem";
            this.reportBugViaGithubToolStripMenuItem.Overflow = System.Windows.Forms.ToolStripItemOverflow.AsNeeded;
            this.reportBugViaGithubToolStripMenuItem.Padding = new System.Windows.Forms.Padding(14);
            this.reportBugViaGithubToolStripMenuItem.Size = new System.Drawing.Size(276, 64);
            this.reportBugViaGithubToolStripMenuItem.Text = "Report Using Github";
            this.reportBugViaGithubToolStripMenuItem.Click += new System.EventHandler(this.reportBugViaGithubToolStripMenuItem_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(542, 134);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(127, 28);
            this.label42.TabIndex = 10;
            this.label42.Text = "Source Code";
            // 
            // addAuditCancelButton
            // 
            this.addAuditCancelButton.BackColor = System.Drawing.Color.Red;
            this.addAuditCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addAuditCancelButton.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.addAuditCancelButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.addAuditCancelButton.Location = new System.Drawing.Point(210, 908);
            this.addAuditCancelButton.Name = "addAuditCancelButton";
            this.addAuditCancelButton.Size = new System.Drawing.Size(143, 40);
            this.addAuditCancelButton.TabIndex = 22;
            this.addAuditCancelButton.Text = "Cancel";
            this.addAuditCancelButton.UseVisualStyleBackColor = false;
            this.addAuditCancelButton.Click += new System.EventHandler(this.addAuditCancelButton_Click);
            // 
            // BugTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1603, 974);
            this.Controls.Add(this.loginPanel);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mainMenu;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "BugTracker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BugTracker - Version 1.0.0";
            this.loginPanel.ResumeLayout(false);
            this.loginPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).EndInit();
            this.panel2.ResumeLayout(false);
            this.bugsPanel.ResumeLayout(false);
            this.bugsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BugsGridView)).EndInit();
            this.editBugPanel.ResumeLayout(false);
            this.editBugPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AuditHistoryDataGridView)).EndInit();
            this.addNewAuditPanel.ResumeLayout(false);
            this.addNewAuditPanel.PerformLayout();
            this.addNewBugPanel.ResumeLayout(false);
            this.addNewBugPanel.PerformLayout();
            this.addBugViaGithubPanel.ResumeLayout(false);
            this.addBugViaGithubPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBoxMain)).EndInit();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.Label gg;
        private System.Windows.Forms.Button loginSubmit;
        private System.Windows.Forms.TextBox loginFirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox loginPinCode;
        private System.Windows.Forms.PictureBox logoBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox logoBoxMain;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem bugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportANewBugToolStripMenuItem;
        private System.Windows.Forms.Button addNewBugSubmit;
        private System.Windows.Forms.TextBox addNewBugSourceCode;
        private System.Windows.Forms.TextBox addNewBugMethod;
        private System.Windows.Forms.TextBox addNewBugClass;
        private System.Windows.Forms.TextBox addNewBugProject;
        private System.Windows.Forms.Panel addNewBugPanel;
        private System.Windows.Forms.Label lastSignInDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox addNewBugVCHashBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox addNewBugContributor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox addNewBugName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel bugsPanel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView BugsGridView;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel editBugPanel;
        private System.Windows.Forms.Label bugEditingPanelTitle;
        private System.Windows.Forms.Label contributorInfoLabel;
        private System.Windows.Forms.ToolStripMenuItem reportBugViaGithubToolStripMenuItem;
        private System.Windows.Forms.Label bugEditingPanelDescription;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label bugEditingPanelStatusLabel;
        private System.Windows.Forms.DataGridView AuditHistoryDataGridView;
        private System.Windows.Forms.Button updateBugDescriptionButton;
        private System.Windows.Forms.WebBrowser bugEditingPanelSourceCodeViewer;
        private System.Windows.Forms.Button addNewAuditButton;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label loginErrorText;
        private System.Windows.Forms.Button logOutButton;
        private System.Windows.Forms.Panel addNewAuditPanel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox addNewBugSourceFile;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox addNewBugEndLine;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox addNewBugStartLine;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button addAuditButton;
        private System.Windows.Forms.TextBox addAuditSourceCode;
        private System.Windows.Forms.TextBox addAuditSourceFile;
        private System.Windows.Forms.TextBox addAuditEndLine;
        private System.Windows.Forms.TextBox addAuditStartLine;
        private System.Windows.Forms.TextBox addAuditMethod;
        private System.Windows.Forms.TextBox addAuditClass;
        private System.Windows.Forms.TextBox addAuditProject;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox addAuditStatus;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox addAuditContributor;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox addAuditVersionControlHash;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel addBugViaGithubPanel;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox addBugUsingGithubSourceCode;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox addBugUsingGithubEndLine;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox addBugUsingGithubStartLine;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox addBugUsingGithubClass;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox addBugUsingGithubMethod;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button addBugUsingGithubSubmitButton;
        private System.Windows.Forms.WebBrowser addBugUsingGithubPreview;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox addBugUsingGithubSourceFile;
        private System.Windows.Forms.ComboBox addBugUsingGithubProject;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.DataGridViewTextBoxColumn BugViewId;
        private System.Windows.Forms.DataGridViewTextBoxColumn BugName;
        private System.Windows.Forms.DataGridViewTextBoxColumn VersionControlHash;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeAuthor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewButtonColumn ViewBugHistory;
        private System.Windows.Forms.DataGridViewButtonColumn RemoveBugBtn;
        private System.Windows.Forms.ComboBox addBugUsingGithubContributor;
        private System.Windows.Forms.TextBox bugEditingPanelDescriptionTextbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuditStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuditProject;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuditClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuditSourceFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuditMethod;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuditLines;
        private System.Windows.Forms.TextBox addBugUsingGithubName;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox addBugUsingGithubCommit;
        private System.Windows.Forms.Button addAuditCancelButton;
        private System.Windows.Forms.Label label42;
    }
}

