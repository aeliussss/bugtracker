﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker
{
    /// <summary>
    /// class Audit
    /// </summary>
    public class Audit 
    {
        /// <summary>
        /// The identifier
        /// </summary>
        public int Id;
        /// <summary>
        /// The contributor identifier
        /// </summary>
        public int ContributorId;
        /// <summary>
        /// The bug identifier
        /// </summary>
        public int BugId;
        /// <summary>
        /// The status
        /// </summary>
        public string Status;
        /// <summary>
        /// The project
        /// </summary>
        public string Project;
        /// <summary>
        /// The class
        /// </summary>
        public string Class;
        /// <summary>
        /// The method
        /// </summary>
        public string Method;
        /// <summary>
        /// The source file
        /// </summary>
        public string SourceFile;
        /// <summary>
        /// The version control hash
        /// </summary>
        public string SourceCode;
        /// <summary>
        /// The version control hash
        /// </summary>
        public string VersionControlHash;
        /// <summary>
        /// The start line
        /// </summary>
        public int StartLine;
        /// <summary>
        /// The end line
        /// </summary>
        public int EndLine;

        /// <summary>
        /// Initializes a new instance of the <see cref="Audit" /> class.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="ContributorId">The contributor identifier.</param>
        /// <param name="BugId">The bug identifier.</param>
        /// <param name="Status">The status.</param>
        /// <param name="Project">The project.</param>
        /// <param name="Class">The class.</param>
        /// <param name="Method">The method.</param>
        /// <param name="SourceFile">The source file.</param>
        /// <param name="SourceCode">The source code.</param>
        /// <param name="StartLine">The start line.</param>
        /// <param name="EndLine">The end line.</param>
        /// <param name="VersionControlHash">The version control hash.</param>
        public Audit (int Id, int ContributorId, int BugId, string Status, string Project, string Class, string Method, string SourceFile, string SourceCode, int StartLine, int EndLine, string VersionControlHash)
        {
            this.Id = Id;
            this.ContributorId = ContributorId;
            this.BugId = BugId;
            this.Status = Status;
            this.Project = Project;
            this.Class = Class;
            this.Method = Method;
            this.SourceFile = SourceFile;
            this.SourceCode = SourceCode;
            this.VersionControlHash = VersionControlHash;
            this.StartLine = StartLine;
            this.EndLine = EndLine;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <returns></returns>
        public int GetId ()
        {
            return Id;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        public void SetId(int Id)
        {
            this.Id = Id;
        }

        /// <summary>
        /// Gets the contributor identifier.
        /// </summary>
        /// <returns></returns>
        public int GetContributorId()
        {
            return Id;
        }

        /// <summary>
        /// Sets the contributor identifier.
        /// </summary>
        /// <param name="ContributorId">The contributor identifier.</param>
        public void SetContributorId(int ContributorId)
        {
            this.ContributorId = ContributorId;
        }

        /// <summary>
        /// Gets the bug identifier.
        /// </summary>
        /// <returns></returns>
        public int GetBugId()
        {
            return BugId;
        }

        /// <summary>
        /// Sets the bug identifier.
        /// </summary>
        /// <param name="BugId">The bug identifier.</param>
        public void SetBugId(int BugId)
        {
            this.BugId = BugId;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <returns></returns>
        public string GetStatus()
        {
            return Status;
        }

        /// <summary>
        /// Sets the status.
        /// </summary>
        /// <param name="Status">The status.</param>
        public void SetStatus(string Status)
        {
            this.Status = Status;
        }

        /// <summary>
        /// Gets the project.
        /// </summary>
        /// <returns></returns>
        public string GetProject()
        {
            return Project;
        }

        /// <summary>
        /// Sets the project.
        /// </summary>
        /// <param name="Project">The project.</param>
        public void SetProject(string Project)
        {
            this.Project = Project;
        }

        /// <summary>
        /// Gets the class.
        /// </summary>
        /// <returns></returns>
        public string GetClass()
        {
            return Class;
        }

        /// <summary>
        /// Sets the class.
        /// </summary>
        /// <param name="Class">The class.</param>
        public void SetClass(string Class)
        {
            this.Class = Class;
        }

        /// <summary>
        /// Gets the method.
        /// </summary>
        /// <returns></returns>
        public string GetMethod()
        {
            return Method;
        }

        /// <summary>
        /// Sets the method.
        /// </summary>
        /// <param name="Method">The method.</param>
        public void SetMethod(string Method)
        {
            this.Method = Method;
        }

        /// <summary>
        /// Gets the source file.
        /// </summary>
        /// <returns></returns>
        public string GetSourceFile()
        {
            return SourceFile;
        }

        /// <summary>
        /// Sets the source file.
        /// </summary>
        /// <param name="SourceFile">The source file.</param>
        public void SetSourceFile(string SourceFile)
        {
            this.SourceFile = SourceFile;
        }

        /// <summary>
        /// Gets the source code.
        /// </summary>
        /// <returns></returns>
        public string GetSourceCode()
        {
            return SourceCode;
        }

        /// <summary>
        /// Sets the source code.
        /// </summary>
        /// <param name="SourceCode">The source code.</param>
        public void SetSourceCode(string SourceCode)
        {
            this.SourceCode = SourceCode;
        }

        /// <summary>
        /// Gets the version control hash.
        /// </summary>
        /// <returns></returns>
        public string GetVersionControlHash()
        {
            return VersionControlHash;
        }

        /// <summary>
        /// Sets the version control hash.
        /// </summary>
        /// <param name="VersionControlHash">The version control hash.</param>
        public void SetVersionControlHash(string VersionControlHash)
        {
            this.VersionControlHash = VersionControlHash;
        }

        /// <summary>
        /// Gets the start line.
        /// </summary>
        /// <returns></returns>
        public int GetStartLine()
        {
            return StartLine;
        }

        /// <summary>
        /// Sets the start line.
        /// </summary>
        /// <param name="StartLine">The start line.</param>
        public void SetStartLine(int StartLine)
        {
            this.StartLine = StartLine;
        }

        /// <summary>
        /// Gets the end line.
        /// </summary>
        /// <returns></returns>
        public int GetEndLine()
        {
            return EndLine;
        }

        /// <summary>
        /// Sets the end line.
        /// </summary>
        /// <param name="EndLine">The end line.</param>
        public void SetEndLine(int EndLine)
        {
            this.EndLine = EndLine;
        }

        /// <summary>
        /// Gets the formatted lines.
        /// </summary>
        /// <returns>A formatted string containg line data.</returns>
        public string GetFormattedLines()
        {
            string lines = GetStartLine() + " - " + GetEndLine();

            return lines;
        }
    }
}
