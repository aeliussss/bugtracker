﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using BugTracker;

namespace BugTracker
{
    /// <summary>
    /// Class Contirbutor
    /// </summary>
    public class Contributor
    {
        /// <summary>
        /// The identifier
        /// </summary>
        public int Id;

        /// <summary>
        /// The first name
        /// </summary>
        public string FirstName;

        /// <summary>
        /// The last name
        /// </summary>
        public string LastName;

        /// <summary>
        /// The role
        /// </summary>
        public string Role;

        /// <summary>
        /// The pin code
        /// </summary>
        public int PinCode;

        /// <summary>
        /// The last sign in
        /// </summary>
        public SqlDateTime LastSignIn;

        /// <summary>
        /// Initializes a new instance of the <see cref="Contributor" /> class.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="FirstName">The first name.</param>
        /// <param name="LastName">The last name.</param>
        /// <param name="Role">The role.</param>
        /// <param name="PinCode">The pin code.</param>
        /// <param name="LastSignIn">The last sign in.</param>
        public Contributor (int Id, string FirstName, string LastName, string Role, int PinCode, SqlDateTime LastSignIn)
        {
            this.Id = Id;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Role = Role;
            this.PinCode = PinCode;
            this.LastSignIn = LastSignIn;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <returns></returns>
        public int GetId ()
        {
            return Id;
        }

        /// <summary>
        /// Sets the identifier.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        public void SetId (int Id)
        {
            this.Id = Id;
        }

        /// <summary>
        /// Gets the pin code.
        /// </summary>
        /// <returns></returns>
        public int GetPinCode()
        {
            return PinCode;
        }

        /// <summary>
        /// Sets the pin code.
        /// </summary>
        /// <param name="PinCode">The pin code.</param>
        public void SetPinCode(int PinCode)
        {
            this.PinCode = PinCode;
        }

        /// <summary>
        /// Gets the first name.
        /// </summary>
        /// <returns></returns>
        public string GetFirstName()
        {
            return FirstName;
        }

        /// <summary>
        /// Sets the first name.
        /// </summary>
        /// <param name="FirstName">The first name.</param>
        public void SetFirstName(string FirstName)
        {
            this.FirstName = FirstName;
        }

        /// <summary>
        /// Gets the last name.
        /// </summary>
        /// <returns></returns>
        public string GetLastName()
        {
            return FirstName;
        }

        /// <summary>
        /// Sets the last name.
        /// </summary>
        /// <param name="LastName">The last name.</param>
        public void SetLastName(string LastName)
        {
            this.LastName = LastName;
        }

        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <returns></returns>
        public string GetRole()
        {
            return Role;
        }

        /// <summary>
        /// Sets the role.
        /// </summary>
        /// <param name="Role">The role.</param>
        public void SetRole(string Role)
        {
            this.Role = Role;
        }

        /// <summary>
        /// Gets the last sign in.
        /// </summary>
        /// <returns></returns>
        public SqlDateTime GetLastSignIn()
        {
            return LastSignIn;
        }

        /// <summary>
        /// Sets the last sign in.
        /// </summary>
        /// <param name="LastSignIn">The last sign in.</param>
        public void SetLastSignIn(SqlDateTime LastSignIn)
        {
            this.LastSignIn = LastSignIn;
        }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <returns>
        /// The full name of the contributor.
        /// </returns>
        public string GetFullName()
        {
            return FirstName + " " + LastName;
        }
    }
}
