﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;

namespace BugTracker
{
    /// <summary>
    /// VersionControl Class
    /// </summary>
    public class VersionControl
    {
        /// <summary>
        /// The user name
        /// </summary>
        private static string githubUserName = "computerstudent";
        /// <summary>
        /// The user name
        /// </summary>
        private static string githubPassword = "Gravy1234*";
        /// <summary>
        /// Retrieves all github commits in a dictionary.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> retrieveAllGithubCommits ()
        {
            // create variables for retrieving github commits
            Dictionary<string,string> commits = new Dictionary<string, string>();
            IList<JToken> projects = retrieveGithubProjects();

            // add commits to the dictionary
            for (int i = 0; i < projects.Count; i++)
            {
                IList<JToken> project_commits = retrieveGithubProjectCommits(projects.ElementAt(i)["name"].ToString());

                for (int z = 0; z < project_commits.Count; z++)
                {
                    commits.Add(project_commits.ElementAt(z)["sha"].ToString(), project_commits.ElementAt(z)["sha"].ToString() + " - " + project_commits.ElementAt(z)["commit"]["message"].ToString());
                }
            }

            return commits;
        }

        /// <summary>
        /// Retrieves the github projects.
        /// </summary>
        /// <returns></returns>
        public static IList<JToken> retrieveGithubProjects ()
        {
            // set variables for retrieving github projects
            string url = "https://api.github.com/users/computerstudent/repos";
            IList<JToken> results = JObject.Parse("{}").Children().ToList();

            // use HttpWebRequest & HttpWebResponse classes to retrieve projects from github
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "GET";
                //request.ContentType = "application/json";
                request.UserAgent = ".NET Framework";
                string authInfo = githubUserName + ":" + githubPassword;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                request.Headers.Add("Authorization", "Basic " + authInfo);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                var encoding = Encoding.ASCII;
                using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                {
                    // convert response into json array
                    JArray versions = JArray.Parse(reader.ReadToEnd());
                    results = versions.Children().ToList();
                }
            } catch (WebException e)
            {
                Debug.Write(e.ToString());
            }

            return results;
        }

        /// <summary>
        /// Retrieves the github project commits.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns></returns>
        public static IList<JToken> retrieveGithubProjectCommits(string project)
        {
            // set variables for retrieving github projects
            string url = "https://api.github.com/repos/computerstudent/" + project + "/commits";
            IList<JToken> results = JObject.Parse("{}").Children().ToList();

            // use HttpWebRequest & HttpWebResponse classes to retrieve projects from github
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "GET";
                //request.ContentType = "application/json";
                request.UserAgent = ".NET Framework";
                string authInfo = githubUserName + ":" + githubPassword;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                request.Headers.Add("Authorization", "Basic " + authInfo);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                var encoding = Encoding.ASCII;
                using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                {
                    // convert response into json array
                    JArray versions = JArray.Parse(reader.ReadToEnd());
                    results = versions.Children().ToList();
                }
            }
            catch (WebException e)
            {
                Debug.Write(e.ToString());
            }

            return results;
        }

        /// <summary>
        /// Retrieves the github files.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns></returns>
        public static IList<JToken> retrieveGithubFiles(string project)
        {
            // set variables for retrieving github projects
            string url = "https://api.github.com/repos/computerstudent/" + project + "/git/trees/master";
            IList<JToken> results = JObject.Parse("{}").Children().ToList();

            // use HttpWebRequest & HttpWebResponse classes to retrieve projects from github
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "GET";
                request.ContentType = "application/json";
                request.UserAgent = ".NET Framework";
                string authInfo = githubUserName + ":" + githubPassword;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                request.Headers.Add("Authorization", "Basic " + authInfo);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                var encoding = Encoding.ASCII;
                using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                {
                    JObject versions = JObject.Parse(reader.ReadToEnd());
                    results = versions["tree"].Children().ToList();
                }
            } catch (WebException e)
            {
                Debug.Write(e.ToString());
            }

            return results;
        }

        /// <summary>
        /// Retrieves the github file contents.
        /// </summary>
        /// <param name="file_path">The file_path.</param>
        /// <param name="project">The project.</param>
        /// <returns></returns>
        public static string retrieveGithubFileContents(string file_path, string project)
        {
            // set variables for retrieving github file contents
            string url = "https://api.github.com/repos/computerstudent/" + project + "/contents/" + file_path;
            string contents = "";

            // use HttpWebRequest & HttpWebResponse classes to retrieve projects from github
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "GET";
                request.ContentType = "application/json";
                request.UserAgent = ".NET Framework";
                string authInfo = githubUserName + ":" + githubPassword;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                request.Headers.Add("Authorization", "Basic " + authInfo);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                var encoding = Encoding.ASCII;
                using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                {
                    JObject versions = JObject.Parse(reader.ReadToEnd());
                    byte[] data = Convert.FromBase64String(versions["content"].ToString());
                    contents = Encoding.UTF8.GetString(data);
                }
            } catch (WebException e)
            {
                Debug.Write(e.ToString());
            }

            return contents;
        }
    }
}
