﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;

namespace BugTracker
{
    /// <summary>
    /// Bug Class
    /// </summary>
    public class Bug
    {
        /// <summary>
        /// The identifier
        /// </summary>
        public int id;
        /// <summary>
        /// The name
        /// </summary>
        public string name;
        /// <summary>
        /// The date created
        /// </summary>
        public SqlDateTime dateCreated;
        /// <summary>
        /// The author identifier
        /// </summary>
        public int contributorId;
        /// <summary>
        /// The original vc hash
        /// </summary>
        public string originalVCHash;
        /// <summary>
        /// The status
        /// </summary>
        public string status;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bug" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="contributorId">The contributor identifier.</param>
        /// <param name="dateCreated">The date created.</param>
        /// <param name="originalVCHash">The original vc hash.</param>
        /// <param name="status">The status.</param>
        public Bug (int id, string name, int contributorId, SqlDateTime dateCreated, string originalVCHash, string status)
        {
            this.id = id;
            this.name = name;
            this.contributorId = contributorId;
            this.dateCreated = dateCreated;
            this.originalVCHash = originalVCHash;
            this.status = status;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <returns></returns>
        public int GetId ()
        {
            return id;
        }

        /// <summary>
        /// Sets the identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void SetId (int id)
        {
            this.id = id;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <returns></returns>
        public string getName()
        {
            return name;
        }

        /// <summary>
        /// Sets the name.
        /// </summary>
        /// <param name="name">The name.</param>
        public void setName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Gets the date created.
        /// </summary>
        /// <returns></returns>
        public SqlDateTime getDateCreated()
        {
            return dateCreated;
        }

        /// <summary>
        /// Sets the date created.
        /// </summary>
        /// <param name="dateCreated">The date created.</param>
        public void setDateCreated(SqlDateTime dateCreated)
        {
            this.dateCreated = dateCreated;
        }

        /// <summary>
        /// Gets the author identifier.
        /// </summary>
        /// <returns></returns>
        public int GetContributorId()
        {
            return contributorId;
        }

        /// <summary>
        /// Sets the author identifier.
        /// </summary>
        /// <param name="contributorId">The contributor identifier.</param>
        public void setContributorId(int contributorId)
        {
            this.contributorId = contributorId;
        }

        /// <summary>
        /// Gets the original vc hash.
        /// </summary>
        /// <returns></returns>
        public string getOriginalVCHash()
        {
            return originalVCHash;
        }

        /// <summary>
        /// Sets the original vc hash.
        /// </summary>
        /// <param name="originalVCHash">The original vc hash.</param>
        public void setOriginalVCHash(string originalVCHash)
        {
            this.originalVCHash = originalVCHash;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <returns></returns>
        public string getStatus()
        {
            return status;
        }

        /// <summary>
        /// Sets the status.
        /// </summary>
        /// <param name="status">The status.</param>
        public void setStatus(string status)
        {
            this.status = status;
        }
    }
}
