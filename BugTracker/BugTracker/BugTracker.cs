﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ColorCode;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlTypes;
using System.Globalization;

namespace BugTracker
{
    /// <summary>
    /// BugTracker class that implements the Form class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class BugTracker : Form
    {
        /// <summary>
        /// The database connection string used
        /// </summary>
        string sqlConnectionString = "Data Source=GEORGEBAKERA350\\SQLEXPRESS;Initial Catalog=BugTracker;Integrated Security=True";
        /// <summary>
        /// The constant used for the bugs grid view label update button
        /// </summary>
        public const string BUGS_GRID_VIEW_UPDATE_LABEL = "Update Bug";
        /// <summary>
        /// The constant used for the bugs grid view label remove button
        /// </summary>
        public const string BUGS_GRID_VIEW_REMOVE_LABEL = "Remove Bug";
        /// <summary>
        /// The bug list containing in memory bug classes
        /// </summary>
        public List<Bug> BugList = new List<Bug>();
        /// <summary>
        /// The contributor list containing in memory classes
        /// </summary>
        public List<Contributor> ContributorList = new List<Contributor>();
        /// <summary>
        /// The contributor list containing in memory classes
        /// </summary>
        public List<Audit> AuditList = new List<Audit>();
        /// <summary>
        /// The current bug class containing all the info for the current bug being edited
        /// </summary>
        public Bug BugBeingEdited;
        /// <summary>
        /// The logged in contributor class containing all the info for the current logged in contributor
        /// </summary>
        public Contributor LoggedInContributor;
        /// <summary>
        /// Initializes a new instance of the <see cref="BugTracker" /> class.
        /// </summary>
        public BugTracker()
        {
            InitializeComponent();

            // hide initial column in grid view components
            BugsGridView.RowHeadersVisible = false;
            AuditHistoryDataGridView.RowHeadersVisible = false;
            
            // change the x y values of certain panels for setup
            loginPanel.Left = 0;
            loginPanel.Top = 0;
            panel2.Top = 0;
            panel2.Left = 0;

            // disable use of maximize and minimize
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;

            // invoke methods that get data from the database and store it in the class lists
            populateBugList();
            populateContributorList();

            // create contributor dictionary for use in ComboBox components
            var dict = new Dictionary<int, string>();

            // populate dictionary with ContributorList
            for (int i = 0; i < ContributorList.Count; i++)
            {
                dict.Add(ContributorList.ElementAt(i).GetId(), ContributorList.ElementAt(i).GetFullName());              
            }

            // set the addNewBugContributor ComboBox variables
            addNewBugContributor.DataSource = new BindingSource(dict, null);
            addNewBugContributor.DisplayMember = "Value";
            addNewBugContributor.ValueMember = "Key";
            addNewBugContributor.SelectedIndex = 0;

            // set the addBugUsingGithubContributor variables
            addBugUsingGithubContributor.DataSource = new BindingSource(dict, null);
            addBugUsingGithubContributor.DisplayMember = "Value";
            addBugUsingGithubContributor.ValueMember = "Key";
            addBugUsingGithubContributor.SelectedIndex = 0;

            // create the statuses dictionary for use in ComboBox components
            var statuses = new Dictionary<string, string>();

            // add statuses listed in specification
            statuses.Add("UNCONFIRMED", "Unconfirmed");
            statuses.Add("CONFIRMED", "Confirmed");
            statuses.Add("IN_PROGRESS", "In Progress");
            statuses.Add("AWAITING_FIX_CONFIRMATION", "Fixed. Awaiting Confirmation.");
            statuses.Add("FIXED", "Fixed");

            // set the addAuditStatus variables
            addAuditStatus.DataSource = new BindingSource(statuses, null);
            addAuditStatus.DisplayMember = "Value";
            addAuditStatus.ValueMember = "Key";
            addAuditStatus.SelectedIndex = 0;

            // create contributor dictionary for use in ComboBox components
            var addNewAuditContributorDict = new Dictionary<int, string>();

            // populate dictionary with ContributorList
            for (int i = 0; i < ContributorList.Count; i++)
            {
                addNewAuditContributorDict.Add(ContributorList.ElementAt(i).GetId(), ContributorList.ElementAt(i).GetFullName());
            }

            // set the addAuditContributor variables
            addAuditContributor.DataSource = new BindingSource(addNewAuditContributorDict, null);
            addAuditContributor.DisplayMember = "Value";
            addAuditContributor.ValueMember = "Key";
            addAuditContributor.SelectedIndex = 0;

            // create contributor dictionary for use in ComboBox components
            var CommitsDict = VersionControl.retrieveAllGithubCommits();

            // set the addNewBugContributor ComboBox variables
            addNewBugVCHashBox.DataSource = new BindingSource(CommitsDict, null);
            addNewBugVCHashBox.DisplayMember = "Value";
            addNewBugVCHashBox.ValueMember = "Key";
            addNewBugVCHashBox.SelectedIndex = 0;

            // invoke the method that updates the BugsGridView DataGridView components
            updateBugsDataGridView();
            
            // create dictionary to store github projects
            var GithubProjectsDict = new Dictionary<string, string>();

            // retrieve projects from github using the VersionControl class
            IList<JToken> projects = VersionControl.retrieveGithubProjects();

            // populate dictionary with ContributorList
            for (int i = 0; i < projects.Count; i++)
            {
                GithubProjectsDict.Add(projects.ElementAt(i)["name"].ToString(), projects.ElementAt(i)["name"].ToString());
            }

            // set the addBugUsingGithubProject variables
            addBugUsingGithubProject.DataSource = new BindingSource(GithubProjectsDict, null);
            addBugUsingGithubProject.DisplayMember = "Value";
            addBugUsingGithubProject.ValueMember = "Key";
            addBugUsingGithubProject.SelectedIndex = 0;

            // create dictionary to store github files from a particular github project
            var GithubFilesDict = new Dictionary<string, string>();

            // retrieve files from github using the VersionControl class
            IList<JToken> files = VersionControl.retrieveGithubFiles(projects.ElementAt(0)["name"].ToString());

            // populate dictionary with files
            for (int i = 0; i < files.Count; i++)
            {
                GithubFilesDict.Add(files.ElementAt(i)["path"].ToString(), files.ElementAt(i)["path"].ToString());
            }

            // set the addBugUsingGithubProject variables
            addBugUsingGithubSourceFile.DataSource = new BindingSource(GithubFilesDict, null);
            addBugUsingGithubSourceFile.DisplayMember = "Value";
            addBugUsingGithubSourceFile.ValueMember = "Key";
            addBugUsingGithubSourceFile.SelectedIndex = 0;

            // retrieve file contents using the VersionControl class and update preview components and catch exceptions
            string code = VersionControl.retrieveGithubFileContents(files.ElementAt(0)["path"].ToString(), addBugUsingGithubProject.SelectedValue.ToString());
            string colorizedSourceCode = new CodeColorizer().Colorize(code, Languages.CSharp);
            addBugUsingGithubPreview.DocumentText = "<html><style>pre {width: 50px; display: block;}</style><body><div style=\"position: relative; width: 100px; font-size: 13px;\">" + colorizedSourceCode + "</div></body></html>";

            // create a dictionary to store github files
            var GithubCommitDict = new Dictionary<string, string>();

            // use the version control class to retrieve github commits related to the projects and add to the dictionary
            IList<JToken> commits = VersionControl.retrieveGithubProjectCommits(projects.ElementAt(0)["name"].ToString());
            for (int i = 0; i < files.Count; i++)
            {
                GithubCommitDict.Add(commits.ElementAt(i)["sha"].ToString(), commits.ElementAt(i)["sha"].ToString() + " - " + commits.ElementAt(i)["commit"]["message"].ToString());
            }

            // bind data to ComboBox addBugUsingGithubCommit and set the selected index to the top item
            addBugUsingGithubCommit.DataSource = new BindingSource(GithubCommitDict, null);
            addBugUsingGithubCommit.DisplayMember = "Value";
            addBugUsingGithubCommit.ValueMember = "Key";
            addBugUsingGithubCommit.SelectedIndex = 0;

            // hide panels and show initial login panel
            loginPanel.Visible = true;
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = false;
            editBugPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            // if user has logged in set applcation state and variables
            if (checkContributorLoginForm() != 0)
            {
                // hide panels
                loginPanel.Visible = false;
                bugsPanel.Visible = true;
                loginErrorText.Visible = false;

                // checkContributorLoginForm() returns the id of the contributor so we can use it to set the current session 
                setCurrentContributor(checkContributorLoginForm());
                contributorInfoLabel.Text = "Welcome " + LoggedInContributor.GetFullName();
                // use a GMT time zone for the sign in date
                CultureInfo ci = new CultureInfo("de-DE");
                DateTime signInDate = DateTime.Parse(LoggedInContributor.GetLastSignIn().ToString(), ci);
                
                lastSignInDate.Text = "Last Signed In: \n" + signInDate.ToString("dd MMM, yyyy - H:mm:ss");
                // record new sign in date
                updateContributorSignIn();
            } else
            {
                // display errors
                loginErrorText.Text = "Your Credentials Are Incorrect! Please try again!";
                loginErrorText.Visible = true;
            } 
        }

        /// <summary>
        /// Sets the current contributor.
        /// </summary>
        /// <param name="ContributorId">The contributor identifier.</param>
        private void setCurrentContributor (int ContributorId)
        {
            // loop through ContributorList to find the logged in contributor's in memory class
            for(int i = 0; i < ContributorList.Count; i++)
            {
                if (ContributorList.ElementAt(i).GetId() == ContributorId)
                {
                    LoggedInContributor = ContributorList.ElementAt(i);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the reportANewBugToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void reportANewBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // hide and show correct panels
            addNewBugPanel.Visible = true;
            bugsPanel.Visible = false;
            editBugPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }

        /// <summary>
        /// Handles the Click event of the addNewBugSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void addNewBugSubmit_Click(object sender, EventArgs e)
        {
            // get field info from form components
            string bugName = addNewBugName.Text;
            int bugId = 0;
            int bugContributor = (int) addNewBugContributor.SelectedValue;
            string bugProject = addNewBugProject.Text;
            string bugClass = addNewBugClass.Text;
            string bugMethod = addNewBugMethod.Text;
            string bugSourceFile = addNewBugSourceFile.Text;
            string bugCommitHash = addNewBugVCHashBox.Text;
            string bugStartLine = addNewBugStartLine.Text;
            string bugEndLine = addNewBugEndLine.Text;
            string bugSourceCode = addNewBugSourceCode.Text;
            int bugStartLineTest;
            int bugEndLineTest;

            //add input empty string verification
            if (string.Empty == bugProject || string.Empty == bugClass || string.Empty == bugMethod || string.Empty == bugSourceFile || string.Empty == bugCommitHash || string.Empty == bugStartLine || string.Empty == bugEndLine || string.Empty == bugSourceCode)
            {
                MessageBox.Show("Please enter all fields.");
                return;
            }

            //add input verification (from unit test)
            try
            {
                bugStartLineTest = int.Parse(bugStartLine);
                bugEndLineTest = int.Parse(bugEndLine);
            }
            catch (FormatException)
            {
                MessageBox.Show("Please enter a number in the start line and end line.");
                return;
            }
            catch (OverflowException)
            {
                MessageBox.Show("Please enter a number in the start line and end line.");
                return;
            }

            // insert a new bug row into the database with the above variables and the Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO Bugs (Name, OriginalContributorID, DateCreated, OriginalVersionControlHash, Status) output INSERTED.ID VALUES ('" + bugName + "','" + bugContributor + "','','" + bugCommitHash + "','UNCONFIRMED');", conn);

                    bugId = (int) command.ExecuteScalar();
                    
                    SqlCommand auditCommand = new SqlCommand("INSERT INTO Audits (ContributorID, BugID, Status, Project, Class, Method, SourceFile, SourceCode, StartLine, EndLine, VersionControlHash) VALUES ('" + bugContributor + "','" + bugId + "','UNCONFIRMED','" + bugProject + "','" + bugClass + "','" + bugMethod + "','" + bugSourceFile + "','" + bugSourceCode + "', '" + bugStartLine + "', '" + bugEndLine + "', '" + bugCommitHash + "')", conn);

                    auditCommand.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }

            // we just added a new bug to the database so we need to add it to our list
            populateBugList();
            // set the variables on the editing view
            updateBugsDataGridView();

            // we want to show the edit screen but beforehand we set the current bug variable & update data grid
            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == bugId)
                {
                    BugBeingEdited = BugList.ElementAt(i);
                }
            }

            // we need to update audit list and data grid view associated with the bug just added
            populateAuditList();
            updateAuditDataGridView();
            
            // update the bug editing view for displaying it
            updateBugEditingView();

            // reset textfields
            addNewBugContributor.SelectedValue = 0;
            addNewBugProject.Text = addNewBugClass.Text = addNewBugMethod.Text = addNewBugSourceFile.Text = addNewBugVCHashBox.Text = addNewBugStartLine.Text = addNewBugEndLine.Text = addNewBugSourceCode.Text = "";

            // switch to the editing view/panel
            editBugPanel.Visible = true;
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }

        /// <summary>
        /// Handles the Click event of the bugsToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void bugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // hide and show correct panels
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = true;
            editBugPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }

        /// <summary>
        /// Populates the bug list.
        /// </summary>
        public void populateBugList()
        {
            // clear the bug list 
            BugList.Clear();

            // select all the bugs listed in the database using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("SELECT * FROM Bugs", conn);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            BugList.Add(new Bug(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetSqlDateTime(3), reader.GetString(4), reader.GetString(5)));
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }
        }

        /// <summary>
        /// Populates the contributor list.
        /// </summary>
        public void populateContributorList()
        {
            // clear the contributor list 
            ContributorList.Clear();

            // select all the contributors listed in the database using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("SELECT * FROM Contributors", conn);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ContributorList.Add(new Contributor(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetInt32(4), reader.GetSqlDateTime(5)));
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }
        }

        /// <summary>
        /// Populates the audit list.
        /// </summary>
        public void populateAuditList()
        {
            // clear the audit list 
            AuditList.Clear();

            // select all the audits listed in the database tha are related to the current bug being edited using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("SELECT * FROM Audits", conn);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //only add audits related to the current bug
                            if (BugBeingEdited.GetId() == reader.GetInt32(2))
                            {
                                AuditList.Add(new Audit(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7), reader.GetString(8), reader.GetInt32(9), reader.GetInt32(10), reader.GetString(11)));
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }
        }

        /// <summary>
        /// Verifies login and returns whether the user has logged in or not.
        /// </summary>
        /// <returns></returns>
        public int checkContributorLoginForm()
        {
            // create variables for login system
            string firstname = loginFirstName.Text.ToString();
            string pincode = loginPinCode.Text;
            int id = 0;

            // select all the rows in the database with the correct credentials using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("SELECT ID,FirstName,PinCode FROM Contributors WHERE FirstName='" + firstname + "' AND PinCode='" + pincode + "'", conn);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            id = reader.GetInt32(0);
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }

            return id;
        }

        /// <summary>
        /// Handles the CellContentClick event of the BugsGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs" /> instance containing the event data.</param>
        private void BugsGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                // update button
                case 5:
                    editBug(e.RowIndex);
                break;
                //remove button
                case 6:
                    for (int i = 0; i < BugList.Count; i++)
                    {
                        if (BugList.ElementAt(i).GetId() == (int) BugsGridView.Rows[e.RowIndex].Cells[0].Value)
                        {
                            // invoke the removeBug method
                            removeBug(BugList.ElementAt(i).GetId());
                        }
                    }
                break;
            }
        }

        /// <summary>
        /// Updates the bug editing view.
        /// </summary>
        private void updateBugEditingView()
        {
            // update the text components for the bugEditingPanel
            bugEditingPanelStatusLabel.Text = "Current Status: " + BugBeingEdited.getStatus();
            bugEditingPanelTitle.Text = "Edit Bug: #" + BugBeingEdited.GetId();
            bugEditingPanelDescription.Text = BugBeingEdited.getName(); 
           
            // get the latest source code form the audit list to display
            string code = AuditList.Last().SourceCode;

            // create the source code with color coding library and the web browser component
            string colorizedSourceCode = new CodeColorizer().Colorize(code, Languages.CSharp);
            bugEditingPanelSourceCodeViewer.DocumentText = "<html><style>pre {width: 50px; display: block;}</style><body><div style=\"position: relative; width: 100px; font-size: 13px;\">" + colorizedSourceCode + "</div></body></html>";

            // update the bug description since this can be updated in the application
            bugEditingPanelDescriptionTextbox.Text = BugBeingEdited.getName();
        }

        /// <summary>
        /// Updates the bugs data grid view.
        /// </summary>
        private void updateBugsDataGridView()
        {
            // clear the rows in the BugsGridView component
            BugsGridView.Rows.Clear();

            // add rows to the BugsGridView component using the BugList 
            for (int i = 0; i < BugList.Count; i++)
            {
                Bug bug = BugList.ElementAt(i);
                string author = "";

                //find contributor name 
                for (int z = 0; z < ContributorList.Count; z++)
                {
                    if (ContributorList.ElementAt(z).GetId() == bug.contributorId)
                    {
                        author = ContributorList.ElementAt(z).GetFullName();
                    }
                }

                BugsGridView.Rows.Add(bug.id, bug.name, bug.originalVCHash, author, bug.status, BUGS_GRID_VIEW_UPDATE_LABEL, BUGS_GRID_VIEW_REMOVE_LABEL);
            }
        }

        /// <summary>
        /// Updates the audit data grid view.
        /// </summary>
        private void updateAuditDataGridView()
        {
            // clear the AuditHistoryDataGridView rows
            AuditHistoryDataGridView.Rows.Clear();

            // loop through the audit list and add audits to the AuditHistoryDataGridView component
            for (int i = 0; i < AuditList.Count; i++)
            {
                Audit audit = AuditList.ElementAt(i);
                string lines = audit.GetFormattedLines();

                AuditHistoryDataGridView.Rows.Add(audit.Status, audit.Project, audit.Class, audit.SourceFile, audit.Method, lines);
            }
        }

        /// <summary>
        /// Removes the bug.
        /// </summary>
        /// <param name="BugId">The bug identifier.</param>
        private void removeBug(int BugId)
        {
            // delete all the rows in the database associated with the bug to be removed using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("DELETE FROM Audits WHERE BugID='" + BugId.ToString() + "'", conn);

                    command.ExecuteNonQuery();

                    SqlCommand bugsCommand = new SqlCommand("DELETE FROM Bugs WHERE ID='" + BugId.ToString() +"'", conn);

                    bugsCommand.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }

            // remove bug from bug list
            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == BugId)
                {
                    BugList.Remove(BugList.ElementAt(i));
                }
            }

            // update bugs data view to remove the bug from the data view
            updateBugsDataGridView();
        }

        /// <summary>
        /// Edits the bug.
        /// </summary>
        /// <param name="index">The index.</param>
        private void editBug (int index)
        {
            // loop through the BugList to find the correct bug to edit 
            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == (int) BugsGridView.Rows[index].Cells[0].Value)
                {
                    BugBeingEdited = BugList.ElementAt(i);
                }
            }

            // update views and lists
            populateAuditList();
            updateAuditDataGridView();
            updateBugEditingView();

            // hide and show the correct panels
            editBugPanel.Visible = true;
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = false;
            addNewAuditPanel.Visible = false;
        }

        /// <summary>
        /// Updates the bug description.
        /// </summary>
        private void updateBugDescription()
        {
            // set variables for updating the description
            int currentBugId = BugBeingEdited.GetId();

            // update the description in the database associated with the bug using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("UPDATE Bugs SET Name='" + bugEditingPanelDescriptionTextbox.Text + "' WHERE ID='" + currentBugId + "'", conn);

                    command.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }

            // update views and lists
            populateBugList();
            updateBugsDataGridView();

            //BugBeingEdited is null since the bug list has been repopulated and therefore doesn't exist so we need to set it again before trying to update the editing view.
            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == currentBugId)
                {
                    BugBeingEdited = BugList.ElementAt(i);
                }
            }

            // update bug editing view after BugBeingEdited has been set
            updateBugEditingView();
        }

        /// <summary>
        /// Updates the current bug status.
        /// </summary>
        /// <param name="BugId">The bug identifier.</param>
        /// <param name="status">The status.</param>
        private void updateCurrentBugStatus (int BugId, string status)
        {
            // set the variables 
            int currentBugId = BugBeingEdited.GetId();

            // update the status column in the database associated with the bug using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("UPDATE Bugs SET Status='" + status + "' WHERE ID='" + currentBugId + "'", conn);

                    command.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }

            // update lists and views
            populateBugList();
            updateBugsDataGridView();

            //BugBeingEdited is null since the bug list has been repopulated and therefore doesn't exist so we need to set it again before trying to update the editing view.
            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == BugId)
                {
                    BugBeingEdited = BugList.ElementAt(i);
                }
            }

            // update bug editing view after BugBeingEdited has been set
            updateBugEditingView();
        }

        /// <summary>
        /// Updates the current bug source code.
        /// </summary>
        /// <param name="BugId">The bug identifier.</param>
        /// <param name="sourceCode">The source code.</param>
        private void updateCurrentBugSourceCode(int BugId, string sourceCode)
        {
            // create the source code with color coding library and the web browser component
            string colorizedSourceCode = new CodeColorizer().Colorize(sourceCode, Languages.CSharp);
            bugEditingPanelSourceCodeViewer.DocumentText = "<html><style>pre {width: 50px; display: block;}</style><body><div style=\"position: relative; width: 100px; font-size: 13px;\">" + colorizedSourceCode + "</div></body></html>";
        }

        /// <summary>
        /// Adds the audit.
        /// </summary>
        private void addAudit ()
        {
            // set the variables for adding audits
            int bugId = BugBeingEdited.GetId();
            string auditProject = addAuditProject.Text;
            string auditClass = addAuditClass.Text;
            string auditMethod = addAuditMethod.Text;
            string auditSourceFile = addAuditSourceFile.Text;
            string auditSourceCode = addAuditSourceCode.Text;
            string auditStatus = addAuditStatus.SelectedValue.ToString();
            string auditStartLine = addAuditStartLine.Text;
            string auditEndLine = addAuditEndLine.Text;
            string auditVersionControlHash = addAuditVersionControlHash.Text;
            int auditCodeContributor = (int) addAuditContributor.SelectedValue;
            int bugStartLineTest;
            int bugEndLineTest;

            //add input empty string verification
            if (string.Empty == auditProject || string.Empty == auditClass || string.Empty == auditMethod || string.Empty == auditSourceFile || string.Empty == auditSourceCode || string.Empty == auditStatus || string.Empty == auditStartLine || string.Empty == auditEndLine || string.Empty == auditVersionControlHash)
            {
                MessageBox.Show("Please enter all fields.");
                return;
            }

            //add input verification (from unit test)
            try
            {
                bugStartLineTest = int.Parse(auditStartLine);
                bugEndLineTest = int.Parse(auditEndLine);
            }
            catch (FormatException)
            {
                MessageBox.Show("Please enter a number in the start line and end line.");
                return;
            }
            catch (OverflowException)
            {
                MessageBox.Show("Please enter a number in the start line and end line.");
                return;
            }

            // insert a row in the Audits table in the database associated with the current bug using Sql C# Library whilst capturing expections
            try
            {

                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO Audits (ContributorID,BugID,Status,Project,Class,Method,SourceFile,SourceCode,StartLine,EndLine,VersionControlHash) VALUES ('" + auditCodeContributor + "','" + bugId + "','" + auditStatus + "','" + auditProject + "','" + auditClass + "','" + auditMethod + "','" + auditSourceFile + "','" + auditSourceCode + "','" + auditStartLine + "','" + auditEndLine + "','" + auditVersionControlHash + "')", conn);

                    command.ExecuteNonQuery();

                    conn.Close();
                }
            } catch (SqlException ex)
            {
                Debug.Write(ex);
            }

            // invoke the updateCurrentBugStatus to update the current bug status in the database
            updateCurrentBugStatus(bugId, auditStatus);

            //invoke the updateCurrentBugSourceCode
            updateCurrentBugSourceCode(bugId, auditSourceCode);

            // update lists and views
            populateAuditList();
            updateAuditDataGridView();

            // show and hide the correct panels
            addNewAuditPanel.Visible = false;
            bugsPanel.Visible = false;
            editBugPanel.Visible = true;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }

        /// <summary>
        /// Handles the Click event of the logOutButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void logOutButton_Click(object sender, EventArgs e)
        {
            // reset textfield components
            loginFirstName.Text = loginPinCode.Text = "";

            // show and hide correct panels
            loginPanel.Visible = true;
            bugsPanel.Visible = false;
            editBugPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }

        /// <summary>
        /// Handles the Click event of the addNewAuditButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void addNewAuditButton_Click(object sender, EventArgs e)
        {
            // show and hide correct panels
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = false;
            editBugPanel.Visible = false;
            addNewAuditPanel.Visible = true;
            addBugViaGithubPanel.Visible = false;

            // set text fields to current data
            addAuditProject.Text = AuditList.Last().GetProject();
            addAuditClass.Text = AuditList.Last().GetProject();
            addAuditMethod.Text = AuditList.Last().GetMethod();
            addAuditSourceFile.Text = AuditList.Last().GetSourceFile();
            addAuditSourceCode.Text = AuditList.Last().GetSourceCode();
            addAuditStartLine.Text = AuditList.Last().GetStartLine().ToString();
            addAuditEndLine.Text = AuditList.Last().GetEndLine().ToString();
            addAuditVersionControlHash.Text = AuditList.Last().VersionControlHash;
        }

        /// <summary>
        /// Updates the contributor sign in.
        /// </summary>
        public void updateContributorSignIn()
        {
            // set the variables to update the sign in date
            DateTime myDateTime = DateTime.Now;
            string signInDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            string currentContributorID = LoggedInContributor.GetId().ToString();

            // 
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();

                SqlCommand command = new SqlCommand("UPDATE Contributors SET LastSignIn='" + signInDate + "' WHERE id='" + currentContributorID + "'", conn);

                command.ExecuteNonQuery();

                conn.Close();
            }

            //set sign in date in contributor list 
            for (int i = 0; i < ContributorList.Count; i++)
            {
                if (ContributorList.ElementAt(i).GetId() == LoggedInContributor.GetId())
                {
                    ContributorList.ElementAt(i).SetLastSignIn(myDateTime);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the addAuditButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void addAuditButton_Click(object sender, EventArgs e)
        {
            addAudit();
        }

        /// <summary>
        /// Handles the Click event of the reportBugViaGithubToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void reportBugViaGithubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = false;
            editBugPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = true;
        }

        /// <summary>
        /// Adds the github bug.
        /// </summary>
        public void addGithubBug()
        {
            // set the variables from the form components to add a github bug
            int bugId = 0;
            string addBugName = addBugUsingGithubName.Text;
            string addBugProject = addBugUsingGithubProject.Text;
            string addBugClass = addBugUsingGithubClass.Text;
            string addBugMethod = addBugUsingGithubMethod.Text;
            int addBugContributor = (int)addBugUsingGithubContributor.SelectedValue;
            string addBugStartLine = addBugUsingGithubStartLine.Text;
            string addBugEndLine = addBugUsingGithubEndLine.Text;
            string addBugSourceFile = addBugUsingGithubSourceFile.Text;
            string addBugSourceCode = addBugUsingGithubSourceCode.Text;
            string addBugOriginalVCHash = addBugUsingGithubCommit.SelectedValue.ToString();
            int bugStartLineTest;
            int bugEndLineTest;

            //add input empty string verification
            if (string.Empty == addBugName || string.Empty == addBugProject || string.Empty == addBugClass || string.Empty == addBugMethod || string.Empty == addBugStartLine || string.Empty == addBugEndLine || string.Empty == addBugSourceFile || string.Empty == addBugSourceCode || string.Empty == addBugOriginalVCHash)
            {
                MessageBox.Show("Please fill in all fields.");
                return;
            }

            //add input verification (from unit test)
            try
            {
                bugStartLineTest = int.Parse(addBugStartLine);
                bugEndLineTest = int.Parse(addBugEndLine);
            }
            catch (FormatException)
            {
                MessageBox.Show("Please enter a number in the start line and end line.");
                return;
            }
            catch (OverflowException)
            {
                MessageBox.Show("Please enter a number in the start line and end line.");
                return;
            }

            // insert a row in the Bugs & Audits table in the database using Sql C# Library whilst capturing expections
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO Bugs (Name, OriginalContributorID, DateCreated, OriginalVersionControlHash, Status) output INSERTED.ID VALUES ('" + addBugName + "','" + addBugContributor + "','','" + addBugOriginalVCHash + "','UNCONFIRMED');", conn);

                    // use the returned bugId to add the audit row's bug id
                    bugId = (int)command.ExecuteScalar();

                    SqlCommand auditCommand = new SqlCommand("INSERT INTO Audits (ContributorID, BugID, Status, Project, Class, Method, SourceFile, SourceCode, StartLine, EndLine, VersionControlHash) VALUES ('" + addBugContributor + "','" + bugId + "','UNCONFIRMED','" + addBugProject + "','" + addBugClass + "','" + addBugMethod + "','" + addBugSourceFile + "','" + addBugSourceCode + "', '" + addBugStartLine + "', '" + addBugEndLine + "', '" + addBugOriginalVCHash + "')", conn);

                    auditCommand.ExecuteNonQuery();

                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Debug.Write(ex);
            }

            // we just added a new bug to the database so we need to add it to our list
            populateBugList();
            // set the variables on the editing view
            updateBugsDataGridView();

            // we want to show the edit screen but beforehand we set the current bug variable & update data grid
            for (int i = 0; i < BugList.Count; i++)
            {
                if (BugList.ElementAt(i).GetId() == bugId)
                {
                    BugBeingEdited = BugList.ElementAt(i);
                }
            }

            // we need to update audit list and data grid view associated with the bug just added
            populateAuditList();
            updateAuditDataGridView();

            // update the bug editing view for displaying it
            updateBugEditingView();

            // reset textfield form components
            addBugUsingGithubProject.Text = "";
            addBugUsingGithubClass.Text = "";
            addBugUsingGithubMethod.Text = "";
            addBugUsingGithubStartLine.Text = "";
            addBugUsingGithubEndLine.Text = "";
            addBugUsingGithubSourceFile.Text = "";
            addBugUsingGithubSourceCode.Text = "";
            addBugUsingGithubCommit.Text = "";

            // switch to the editing view/panel
            editBugPanel.Visible = true;
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }

        /// <summary>
        /// Handles the Click event of the addBugUsingGithubSubmitButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void addBugUsingGithubSubmitButton_Click(object sender, EventArgs e)
        {
            // invoke the addGithubBug method
            addGithubBug();
        }

        /// <summary>
        /// Handles the Click event of the updateBugDescriptionButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void updateBugDescriptionButton_Click(object sender, EventArgs e)
        {
            // switch between editing and preview views for the bug description
            if (bugEditingPanelDescriptionTextbox.Visible == true)
            {
                updateBugDescriptionButton.Text = "Update Bug Description";
                bugEditingPanelDescriptionTextbox.Visible = false;

                // invoke the updateBugDescription method
                updateBugDescription();
            } else
            {
                bugEditingPanelDescriptionTextbox.Visible = true;
                updateBugDescriptionButton.Text = "Save Bug Description";
            }
        }

        /// <summary>
        /// Handles the SelectionChangeCommitted event of the addBugUsingGithubProject control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void addBugUsingGithubProject_SelectionChangeCommitted(object sender, EventArgs e)
        {
            // get github commits and catch exceptions
            try
            {
                // create a dictionary to store github files
                var GithubFilesDict = new Dictionary<string, string>();
                IList<JToken> files = JObject.Parse("{}").Children().ToList();

                files = VersionControl.retrieveGithubFiles(addBugUsingGithubProject.SelectedValue.ToString());

                for (int i = 0; i < files.Count; i++)
                {
                    GithubFilesDict.Add(files.ElementAt(i)["path"].ToString(), files.ElementAt(i)["path"].ToString());
                }

                // bind data to ComboBox addBugUsingGithubSourceFile and set the selected index to the top item
                addBugUsingGithubSourceFile.DataSource = new BindingSource(GithubFilesDict, null);
                addBugUsingGithubSourceFile.DisplayMember = "Value";
                addBugUsingGithubSourceFile.ValueMember = "Key";
                addBugUsingGithubSourceFile.SelectedIndex = 0;

                // retrieve file contents using the VersionControl class and update preview components and catch exceptions
                string code = VersionControl.retrieveGithubFileContents(addBugUsingGithubSourceFile.SelectedValue.ToString(), addBugUsingGithubProject.SelectedValue.ToString());
                string colorizedSourceCode = new CodeColorizer().Colorize(code, Languages.CSharp);
                addBugUsingGithubPreview.DocumentText = "<html><style>pre {width: 50px; display: block;}</style><body><div style=\"position: relative; width: 100px; font-size: 13px;\">" + colorizedSourceCode + "</div></body></html>";

                // create a dictionary to store github files
                var GithubCommitDict = new Dictionary<string, string>();

                // use the version control class to retrieve github commits related to the projects and add to the dictionary
                IList<JToken> commits = VersionControl.retrieveGithubProjectCommits(addBugUsingGithubProject.SelectedValue.ToString());
                for (int i = 0; i < files.Count; i++)
                {
                    GithubCommitDict.Add(commits.ElementAt(i)["sha"].ToString(), commits.ElementAt(i)["sha"].ToString() + " - " + commits.ElementAt(i)["commit"]["message"].ToString());
                }

                // bind data to ComboBox addBugUsingGithubCommit and set the selected index to the top item
                addBugUsingGithubCommit.DataSource = new BindingSource(GithubCommitDict, null);
                addBugUsingGithubCommit.DisplayMember = "Value";
                addBugUsingGithubCommit.ValueMember = "Key";
                addBugUsingGithubCommit.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
            }
        }

        /// <summary>
        /// Handles the SelectionChangeCommitted event of the addBugUsingGithubSourceFile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void addBugUsingGithubSourceFile_SelectionChangeCommitted(object sender, EventArgs e)
        {
            // retrieve file contents using the VersionControl class and update preview components and catch exceptions
            try
            {
                string code = VersionControl.retrieveGithubFileContents(addBugUsingGithubSourceFile.SelectedValue.ToString(), addBugUsingGithubProject.SelectedValue.ToString());
                string colorizedSourceCode = new CodeColorizer().Colorize(code, Languages.CSharp);
                addBugUsingGithubPreview.DocumentText = "<html><style>pre {width: 50px; display: block;}</style><body><div style=\"position: relative; width: 100px; font-size: 13px;\">" + colorizedSourceCode + "</div></body></html>";
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the addAuditCancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void addAuditCancelButton_Click(object sender, EventArgs e)
        {
            editBugPanel.Visible = true;
            addNewBugPanel.Visible = false;
            bugsPanel.Visible = false;
            addNewAuditPanel.Visible = false;
            addBugViaGithubPanel.Visible = false;
        }
    }
}